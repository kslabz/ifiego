﻿<?php require_once("includes/initialize.php"); ?>
<?php

$providerO = Provider::getInstance();
$provider_id = isset($_GET['id']) ? $_GET['id'] : 0;

$provider = $providerO->find_by_id($provider_id);
if (!$provider) {
	redirect_to('index.php');
}



$options1 = '';
$options2 = '';
if (!strcmp($_SERVER['REQUEST_METHOD'],'POST')) {

		$date = $_POST['check_in_date'];

		$check_in_time = isset($_POST['check_in_time']) ? $_POST['check_in_time'] : '';
		$check_out_time = isset($_POST['check_out_time']) ? $_POST['check_out_time'] : '';
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];



		if (strlen($date)) {
			$day = date('D',strtotime($date));



		    switch ($day) {
		        case 'Mon': $schedule = $provider->monday_schedule; break;
		        case 'Tue': $schedule = $provider->tuesday_schedule; break;
		        case 'Wed': $schedule = $provider->wednesday_schedule; break;
		        case 'Thu': $schedule = $provider->thursday_schedule; break;
		        case 'Fri': $schedule = $provider->friday_schedule; break;
		        case 'Sat': $schedule = $provider->saturday_schedule; break;
		        case 'Sun': $schedule = $provider->sunday_schedule; break;
		        default: $schedule = '';
		    }


			if (strlen($schedule) && $schedule!= '00:00-00:00'){
			    $hours = explode('-', $schedule);
			    $start = explode(':', $hours[0]);
			    $start_hour = (int) $start[0];
			    $end = explode(':', $hours[1]);
			    $end_hour = (int) $end[0];

                while(strtotime($hours[0]) <= strtotime($hours[1])){
                    $prev = date('H:i', strtotime($hours[0])); // format the start time

                    $next = strtotime('+15mins', strtotime($hours[0])); // add 15 mins
                    $hours[0] = date('H:i', $next);

                    $options1 .='<option value="'.($prev).'"  '.($prev == $check_in_time ? 'selected="selected"' : '').'>'. $prev .'</option>';
                    $options2 .='<option value="'.($prev).'" '.($prev == $check_out_time ? 'selected="selected"' : '').'>'. $prev .'</option>';

                }

			}
		}


		//validate
		$errors = array();

		if ($first_name === ''){
			$errors[] = '<li>First Name is a required field</li>';
		}
		if ($last_name === ''){
			$errors[] = '<li>Last Name is a required field</li>';
		}
		if ($email === ''){
			$errors[] = '<li>Email Address is a required field</li>';
		} else {
			$pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/";
			if (!(preg_match($pattern, $email))) {
				$errors[] = '<li>Email Address is not a valid email address</li>';
			}
		}
		if ($phone === ''){
			$errors[] = '<li>Phone Number is a required field</li>';
		}

		if ($date === ''){
			$errors[] = '<li>Day is a required field</li>';
		}
		if ($check_in_time === '' || $check_out_time === ''){
			$errors[] = '<li>Please choose another day</li>';
		}

		if ((int)$check_in_time > 0 && (int) $check_out_time > 0 ){
			if ($date == date('m/d/Y') && (int)$check_in_time <= ((int)date('H') +2)){
				$errors[] = '<li>Check In Time must be in future with at least 2 hours</li>';
			}

			if ((int)$check_in_time >= (int) $check_out_time) {
				$errors[] = '<li>Check In Time must be lower than Check Out Time</li>';
			}


		}


		if (empty($errors)){
			//send mail


			$style_content = 'style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; padding: 5px 0;"';
			$provider = $providerO->find_by_id($provider_id);



		$html ='
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
			"http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<title>Reservation</title>
		<style type="text/css">

		</style>
		</head>

		<body >
			<table width="602" border="0" cellspacing="0" cellpadding="0" style="border: 1px dashed #BBBBBB; padding: 20px;">
				<tr>
					<td style="border-bottom: 1px solid #BBBBBB; margin-bottom: 10px;">Searchable Address</td>
				</tr>
				<tr>
					<td '.$style_content.'>Provider: '.$provider->first_name.' '.$provider->last_name.'</td>
				</tr>
				<tr>
					<td '.$style_content.'>First Name: '.$first_name.'</td>
				</tr>
				<tr>
					<td '.$style_content.'>Last Name: '.$last_name.'</td>
				</tr>

				<tr>
					<td '.$style_content.'>Email : '.$email.'</td>
				</tr>
				<tr>
					<td '.$style_content.'>Phone: '.$phone.'</td>
				</tr>
				<tr>
					<td '.$style_content.'>Day: '.$date.'</td>
				</tr>
				<tr>
					<td '.$style_content.'>Time: ' . $check_in_time .'-' . $check_out_time . '</td>
				</tr>
				<tr>
					<td '.$style_content.'>Rate per minute: $' . $provider->rate_per_minute .'</td>
				</tr>

			</table>
		</body>
	</html>';


		$subject = "New Reservation";

		// mostly the same variables as before
	    // ($to_name & $from_name are new, $headers was omitted)
	    $to_name = EMAIL_NAME;
	    $to = EMAIL_TO;
	    $from_name = EMAIL_FROM_NAME;
	    $from = EMAIL_FROM;

	    // PHPMailer's Object-oriented approach
	    /*$mail = new PHPMailer();

	    // Can use SMTP
	    // comment out this section and it will use PHP mail() instead
	    //$mail->IsSMTP();
	    //$mail->Host       = EMAIL_HOST;
	    //$mail->SMTPSecure = EMAIL_SMTPSECURE;
	    //$mail->Port       = EMAIL_PORT;
	    //$mail->SMTPAuth   = EMAIL_SMTPAUTH;
	    //$mail->Username   = EMAIL_USERNAME;
	    //$mail->Password   = EMAIL_PASSWORD;

	    $mail->addCustomHeader("Content-Type: text/html; charset=utf8");

	    // Could assign strings directly to these, I only used the
	    // former variables to illustrate how similar the two approaches are.
	    $mail->FromName = $from_name;
	    $mail->From     = $from;
	    $mail->AddAddress($to, $to_name);
	    $mail->Subject  = $subject;
	    $mail->Body     = $html;

	    $result = $mail->Send();*/
		
		$headers = "From: {$from}\n";
		$headers .= "Reply-To: {$from}\n";
		$headers .= "X-Mailer: PHP/".phpversion()."\n";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-Type: text/html; charset=utf8";
		
		$result = mail($to, $subject, $html, $headers);
		//echo $result ? 'Sent' : 'Error'; exit();

	 	// Success
		$session->message("Thank you for the reservation. You will be contacted soon.");
		redirect_to('reserve.php?id='.$provider_id);
	}

}


?>


<?php include_layout_template('head_includes.php'); ?>
<?php include_layout_template('header.php'); ?>

<div id="page" class="container">
    <div id="content">
        <h3>Reserve this parking</h3>
        <p>
        	Provider: <?php echo $provider->first_name.' '.$provider->last_name; ?> <br>
        	Address: <?php echo $provider->address.', '.$provider->city. ', '.$provider->postcode; ?><br>
        	Rate per minute: $<?php echo $provider->rate_per_minute ; ?><br>
        	Schedule: Monday: <?php echo schedule_display($provider->monday_schedule); ?><br>
        	Tuesday: <?php echo schedule_display($provider->tuesday_schedule); ?><br>
        	Wednesday: <?php echo schedule_display($provider->wednesday_schedule); ?><br>
        	Thursday: <?php echo schedule_display($provider->thursday_schedule); ?><br>
        	Friday: <?php echo schedule_display($provider->friday_schedule); ?><br>
        	Saturday: <?php echo schedule_display($provider->saturday_schedule); ?><br>
        	Sunday: <?php echo schedule_display($provider->sunday_schedule); ?><br>
        </p>

        <?php if ( strlen($session->message())): ?>
            <!-- Message OK -->
            <div class="msg msg-ok">
                <p><strong><?php echo $session->message(); ?></strong></p>
            </div>
            <!-- End Message OK -->
        <?php endif; ?>

        <?php if (!empty($errors)): ?>
            <ul class="form-errors">
                <?php foreach ($errors as $error) : ?>
                    <?php echo $error; ?>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>

        <form action="<?php echo $_SERVER['PHP_SELF']; ?>?id=<?php echo $provider_id; ?>" method="post" id="form-provider">

            <!-- Form -->
            <div class="form">


                <p class="clear"></p>
                <p class="inline-field">
                    <div class="left margin-right" >
                        <label>First Name</label>
                        <input type="text" name="first_name" class="field size4" value="<?php echo isset($first_name) ? $first_name: '' ?>" />
                    </div>

                    <div class="left margin-right">
                        <label>Last Name</label>
                        <input type="text" name="last_name" class="field size4" value="<?php echo isset($last_name) ? $last_name: '' ?>" />
                    </div>
                </p>
                <p class="clear"></p>

                <p class="inline-field">
                    <div class="left margin-right" >
                        <label>Phone</label>
                        <input type="text" name="phone" class="field size4" value="<?php echo isset($phone) ? $phone: '' ?>" />
                    </div>
                    <div class="left margin-right" >
                        <label>Email</label>
                        <input type="text" name="email" class="field size4" value="<?php echo isset($email) ? $email: '' ?>" />
                    </div>
                </p>
                <p class="clear"></p>



                <p class="inline-field">
                    <div class="left margin-right" >
                        <label>Day</label>
                        <input class="field size4" type="text" id="check-in-date" name="check_in_date" value="<?php echo (isset($date) ?  $date : '') ;?>" readonly="true" >
                    </div>


                    <div class="left margin-right" >
                        <label>Check In Time</label>
                    	<select name="check_in_time" class="field size2" id="check-in-time" >
                    		<?php echo strlen($options1) ? $options1 :'<option value="">Not Available</option>';?>

                    	</select>
                    </div>
                    <div class="left" >
                        <label>Check Out Time</label>
                    	<select name="check_out_time" class="field size2" id="check-out-time" >
                    		<?php echo strlen($options2) ? $options2 :'<option value="">Not Available</option>';?>
                    	</select>
                    </div>


                </p>
                <p class="clear"></p>
                <br>

            </div>
            <!-- End Form -->

            <!-- Form Buttons -->
            <div class="buttons">

                <input type="submit" class="button" value="submit"/>
            </div>
            <!-- End Form Buttons -->
        </form>

        <script type="text/javascript">
			$(document).ready(function(){
				$("#check-in-date").datepicker({
					minDate: 0,
					onSelect: function(date){

						var parameters = {
							"provider_id": <?php echo $provider_id; ?>,
							"date": date
						};

						$.ajax({
				              type: "Post",
				              dataType: "json",
				              url: "ajax_dates.php",
				              data: parameters,
				              success: function(data)
				              {

				                  if (data.length !== 0){
				                  	$('#check-in-time').html(data.start);
				                  	$('#check-out-time').html(data.end);
				                  }
				                  else {
				                  	$('#check-in-time').html('<option value="">Not Available</option>');
				                  	$('#check-out-time').html('<option value="">Not Available</option>');
				                  }
				              }
				         });

					}
				});

			});
		</script>
    </div>
    <div id="sidebar">
        <h2>Sidebar</h2>
    </div>
</div>

<?php include_layout_template('footer.php'); ?>













