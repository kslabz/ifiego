<?php require_once("../includes/initialize.php"); ?>

<?php if (!$session->is_logged_in()) {

    redirect_to("login.php");

} ?>

<?php

// must have an ID

if (empty($_GET['id'])) {

    $session->message("No User ID was provided.");

}else{

$user = User::find_by_id($_GET['id']);

if ($user && $user->delete()) {

    $session->message("The User {$user->first_name} {$user->last_name} was deleted.");

} else {

    $session->message("The User could not be deleted.");


}

}
$page = 'user.php';
if($user->type == 'admin'){
    $page = 'admin_user.php';
}
redirect_to($page);
redirect_to('user.php');
?>

<?php if (isset($database)) {

    $database->close_connection();

} ?>