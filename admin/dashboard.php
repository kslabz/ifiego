<?php
require_once('../includes/initialize.php');
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}
$userO = User::getInstance();
$user = $userO->find_by_id($session->user_id);
$id_provider = isset($_GET['id']) ? $_GET['id'] : 0;
$msg = isset($_GET['msg']) ? $_GET['msg'] : '';

$add_edit_msg = "Add New";
if ($id_provider > 0) {
    $add_edit_msg = "Edit";
}

$hours = get_hours();
$minutes = get_minutes();

//$schedule_format = "Use this format: hh:mm-hh:mm<br>For not available use 00:00-00:00";
$schedule_format = "For not available leave 00 everywhere";
$additional_info = '<a href="#" class="tooltip" title="'.$schedule_format.'"><img src="'.rtrim($base_url,'/').'/media/admin/images/info-icon.png" alt="" /></a>';
$pattern_schedule = "/^([0-9][0-9]:[0-9][0-9]-[0-9][0-9]:[0-9][0-9])$/";
if (!strcmp($_SERVER['REQUEST_METHOD'],'POST')) {
    $first_name         = trim(strip_tags($_POST['first_name']));
    $last_name          = trim(strip_tags($_POST['last_name']));
    $phone              = trim(strip_tags($_POST['phone']));
    $email              = trim(strip_tags($_POST['email']));
    $address            = trim(strip_tags($_POST['address']));
    $city               = trim(strip_tags($_POST['city']));
    $postcode           = trim(strip_tags($_POST['postcode']));
    if ($_POST['id'] > 0) {
        $latitude             = trim(strip_tags($_POST['latitude']));
        $longitude            = trim(strip_tags($_POST['longitude']));
    }

    $access_information   = trim(strip_tags($_POST['access_information']));
    $monday_from_hours    = $_POST['monday_schedule_from_hours'];
    $monday_from_minutes  = $_POST['monday_schedule_from_minutes'];
    $monday_to_hours      = $_POST['monday_schedule_to_hours'];
    $monday_to_minutes    = $_POST['monday_schedule_to_minutes'];

    $tuesday_from_hours    = $_POST['tuesday_schedule_from_hours'];
    $tuesday_from_minutes  = $_POST['tuesday_schedule_from_minutes'];
    $tuesday_to_hours      = $_POST['tuesday_schedule_to_hours'];
    $tuesday_to_minutes    = $_POST['tuesday_schedule_to_minutes'];

    $wednesday_from_hours    = $_POST['wednesday_schedule_from_hours'];
    $wednesday_from_minutes  = $_POST['wednesday_schedule_from_minutes'];
    $wednesday_to_hours      = $_POST['wednesday_schedule_to_hours'];
    $wednesday_to_minutes    = $_POST['wednesday_schedule_to_minutes'];

    $thursday_from_hours    = $_POST['thursday_schedule_from_hours'];
    $thursday_from_minutes  = $_POST['thursday_schedule_from_minutes'];
    $thursday_to_hours      = $_POST['thursday_schedule_to_hours'];
    $thursday_to_minutes    = $_POST['thursday_schedule_to_minutes'];

    $friday_from_hours    = $_POST['friday_schedule_from_hours'];
    $friday_from_minutes  = $_POST['friday_schedule_from_minutes'];
    $friday_to_hours      = $_POST['friday_schedule_to_hours'];
    $friday_to_minutes    = $_POST['friday_schedule_to_minutes'];

    $saturday_from_hours    = $_POST['saturday_schedule_from_hours'];
    $saturday_from_minutes  = $_POST['saturday_schedule_from_minutes'];
    $saturday_to_hours      = $_POST['saturday_schedule_to_hours'];
    $saturday_to_minutes    = $_POST['saturday_schedule_to_minutes'];

    $sunday_from_hours    = $_POST['sunday_schedule_from_hours'];
    $sunday_from_minutes  = $_POST['sunday_schedule_from_minutes'];
    $sunday_to_hours      = $_POST['sunday_schedule_to_hours'];
    $sunday_to_minutes    = $_POST['sunday_schedule_to_minutes'];
    /*$monday_schedule      = trim(strip_tags($_POST['monday_schedule']));
    $tuesday_schedule      = trim(strip_tags($_POST['tuesday_schedule']));
    $wednesday_schedule      = trim(strip_tags($_POST['wednesday_schedule']));
    $thursday_schedule      = trim(strip_tags($_POST['thursday_schedule']));
    $friday_schedule      = trim(strip_tags($_POST['friday_schedule']));
    $saturday_schedule      = trim(strip_tags($_POST['saturday_schedule']));
    $sunday_schedule      = trim(strip_tags($_POST['sunday_schedule']));*/
    $rate_per_hour     = trim($_POST['rate_per_hour']);
    $rate_first_hour   = trim($_POST['rate_first_hour']);
    $rate_per_day     = trim($_POST['rate_per_day']);
    $rate_first_day   = trim($_POST['rate_first_day']);
    //$booking = trim($_POST['booking']);
    $full_day = trim($_POST['full_day']);
    $public = trim($_POST['public']);

    $errors = array();
    if ($first_name === ''){
        $errors[] = '<li>First Name is a required field</li>';
    }
    if ($last_name === ''){
        $errors[] = '<li>Last Name is a required field</li>';
    }
    if ($phone === ''){
        $errors[] = '<li>Phone is a required field</li>';
    }
    if ($email === ''){
        $errors[] = '<li>Email is a required field</li>';
    } elseif (strlen($email)){
        $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/";
        if (!(preg_match($pattern, $email))) {
            $errors[] = '<li>Email is not a valid email address</li>';
        }
    }
    if ($address === ''){
        $errors[] = '<li>Address is a required field</li>';
    }
    if ($city === ''){
        $errors[] = '<li>City is a required field</li>';
    }
    if ($postcode === ''){
        $errors[] = '<li>Postcode is a required field</li>';
    }
    if ($_POST['id'] > 0) {
        if ($longitude === ''){
            $errors[] = '<li>Longitude is a required field</li>';
        }
        if ($latitude === ''){
            $errors[] = '<li>Latitude is a required field</li>';
        }
    }
    if ($access_information === ''){
        $errors[] = '<li>Access Information is a required field</li>';
    }
    if ($monday_from_hours && ($monday_from_hours!= '00' || $monday_to_hours != '00' || $monday_from_minutes != '00' || $monday_to_minutes != '00' ) && ((int)$monday_from_hours >= (int)$monday_to_hours)){
        $errors[] = '<li>Monday From Hours must be lower than Monday To Hours</li>';
    }
    if ($tuesday_from_hours && ($tuesday_from_hours!= '00' || $tuesday_to_hours != '00' || $tuesday_from_minutes != '00' || $tuesday_to_minutes != '00' ) && ((int)$tuesday_from_hours >= (int)$tuesday_to_hours)){
        $errors[] = '<li>Tuesday From Hours must be lower than Monday To Hours</li>';
    }
    if ( $wednesday_from_hours && ($wednesday_from_hours!= '00' || $wednesday_to_hours != '00' || $wednesday_from_minutes != '00' || $wednesday_to_minutes != '00' ) && ((int)$wednesday_from_hours >= (int)$wednesday_to_hours)){
        $errors[] = '<li>Wednesday From Hours must be lower than Monday To Hours</li>';
    }
    if ($thursday_from_hours && ($thursday_from_hours!= '00' || $thursday_to_hours != '00' || $thursday_from_minutes != '00' || $thursday_to_minutes != '00' ) && ((int)$thursday_from_hours >= (int)$thursday_to_hours)){
        $errors[] = '<li>Thursday From Hours must be lower than Monday To Hours</li>';
    }
    if ( $friday_from_hours && ($friday_from_hours!= '00' || $friday_to_hours != '00' || $friday_from_minutes != '00' || $friday_to_minutes != '00' ) && ((int)$friday_from_hours >= (int)$friday_to_hours)){
        $errors[] = '<li>Friday From Hours must be lower than Monday To Hours</li>';
    }
    if ( $saturday_from_hours && ($saturday_from_hours!= '00' || $saturday_to_hours != '00' || $saturday_from_minutes != '00' || $saturday_to_minutes != '00' ) && ((int)$saturday_from_hours >= (int)$saturday_to_hours)){
        $errors[] = '<li>Saturday From Hours must be lower than Monday To Hours</li>';
    }
    if ($sunday_from_hours && ($sunday_from_hours!= '00' || $sunday_to_hours != '00' || $sunday_from_minutes != '00' || $sunday_to_minutes != '00' ) && ((int)$sunday_from_hours >= (int)$sunday_to_hours)){
        $errors[] = '<li>Sunday From Hours must be lower than Monday To Hours</li>';
    }
    /*if ($monday_schedule === ''){
        $errors[] = '<li>Monday Schedule is a required field</li>';
    }elseif (strlen($monday_schedule)){
        if (!(preg_match($pattern_schedule, $monday_schedule))) {
            $errors[] = '<li>Monday Schedule does not respect the pattern: hh:mm-hh:mm</li>';
        }
    }
    if ($tuesday_schedule === ''){
        $errors[] = '<li>Tuesday Schedule is a required field</li>';
    }elseif (strlen($tuesday_schedule)){
        if (!(preg_match($pattern_schedule, $tuesday_schedule))) {
            $errors[] = '<li>Tuesday Schedule does not respect the pattern: hh:mm-hh:mm</li>';
        }
    }
     if ($wednesday_schedule === ''){
        $errors[] = '<li>Wednesday Schedule is a required field</li>';
    }elseif (strlen($wednesday_schedule)){
        if (!(preg_match($pattern_schedule, $wednesday_schedule))) {
            $errors[] = '<li>Wednesday Schedule does not respect the pattern: hh:mm-hh:mm</li>';
        }
    }
    if ($thursday_schedule === ''){
        $errors[] = '<li>Thursday Schedule is a required field</li>';
    }elseif (strlen($thursday_schedule)){
        if (!(preg_match($pattern_schedule, $thursday_schedule))) {
            $errors[] = '<li>Thursday Schedule does not respect the pattern: hh:mm-hh:mm</li>';
        }
    }
    if ($friday_schedule === ''){
        $errors[] = '<li>Friday Schedule is a required field</li>';
    }elseif (strlen($friday_schedule)){
        if (!(preg_match($pattern_schedule, $friday_schedule))) {
            $errors[] = '<li>Friday Schedule does not respect the pattern: hh:mm-hh:mm</li>';
        }
    }
    if ($saturday_schedule === ''){
        $errors[] = '<li>Saturday Schedule is a required field</li>';
    }elseif (strlen($saturday_schedule)){
        if (!(preg_match($pattern_schedule, $saturday_schedule))) {
            $errors[] = '<li>Saturday Schedule does not respect the pattern: hh:mm-hh:mm</li>';
        }
    }
    if ($sunday_schedule === ''){
        $errors[] = '<li>Sunday Schedule is a required field</li>';
    }elseif (strlen($sunday_schedule)){
        if (!(preg_match($pattern_schedule, $sunday_schedule))) {
            $errors[] = '<li>Sunday Schedule does not respect the pattern: hh:mm-hh:mm</li>';
        }
    }*/
    if($full_day == 'No'){
        if ($rate_per_hour === ''){
            $errors[] = '<li>Rate per hour is a required field</li>';
        }
        else {
            $pattern = "/^(\d+)(\.\d+)?$/";
            if (!(preg_match($pattern, $rate_per_hour))) {
                $errors[] = '<li>Rate per hour is not a valid decimal! Examples: 4, 5.4 </li>';
            }
        }
        if ($rate_first_hour === ''){
            $errors[] = '<li>Rate For first hour is a required field</li>';
        }
        else {
            $pattern = "/^(\d+)(\.\d+)?$/";
            if (!(preg_match($pattern, $rate_first_hour))) {
                $errors[] = '<li>Rate For first hour is not a valid decimal! Examples: 4, 5.4 </li>';
            }
        }
    }else{
        if ($rate_per_day === ''){
            $errors[] = '<li>Rate per day is a required field</li>';
        }
        else {
            $pattern = "/^(\d+)(\.\d+)?$/";
            if (!(preg_match($pattern, $rate_per_day))) {
                $errors[] = '<li>Rate per day is not a valid decimal! Examples: 4, 5.4 </li>';
            }
        }
        if ($rate_first_day === ''){
            $errors[] = '<li>Rate For first day is a required field</li>';
        }
        else {
            $pattern = "/^(\d+)(\.\d+)?$/";
            if (!(preg_match($pattern, $rate_first_day))) {
                $errors[] = '<li>Rate For first day is not a valid decimal! Examples: 4, 5.4 </li>';
            }
        }
    }

    if (empty($errors)){
        $provider          = Provider::getInstance();

        $provider->first_name = $first_name;
        $provider->last_name = $last_name;
        $provider->phone = $phone;
        $provider->email = $email;
        $provider->address = $address;
        $provider->city = $city;
        $provider->postcode = $postcode;

        $provider->access_information = $access_information;
        $provider->monday_schedule = prepare_schedule_for_db($_POST['monday_schedule_from_hours'],$_POST['monday_schedule_from_minutes'], $_POST['monday_schedule_to_hours'], $_POST['monday_schedule_to_minutes'] );
        $provider->tuesday_schedule = prepare_schedule_for_db($_POST['tuesday_schedule_from_hours'],$_POST['tuesday_schedule_from_minutes'], $_POST['tuesday_schedule_to_hours'], $_POST['tuesday_schedule_to_minutes'] );
        $provider->wednesday_schedule = prepare_schedule_for_db($_POST['wednesday_schedule_from_hours'],$_POST['wednesday_schedule_from_minutes'], $_POST['wednesday_schedule_to_hours'], $_POST['wednesday_schedule_to_minutes'] );
        $provider->thursday_schedule = prepare_schedule_for_db($_POST['thursday_schedule_from_hours'],$_POST['thursday_schedule_from_minutes'], $_POST['thursday_schedule_to_hours'], $_POST['thursday_schedule_to_minutes'] );
        $provider->friday_schedule = prepare_schedule_for_db($_POST['friday_schedule_from_hours'],$_POST['friday_schedule_from_minutes'], $_POST['friday_schedule_to_hours'], $_POST['friday_schedule_to_minutes'] );
        $provider->saturday_schedule = prepare_schedule_for_db($_POST['saturday_schedule_from_hours'],$_POST['saturday_schedule_from_minutes'], $_POST['saturday_schedule_to_hours'], $_POST['saturday_schedule_to_minutes'] );
        $provider->sunday_schedule = prepare_schedule_for_db($_POST['sunday_schedule_from_hours'],$_POST['sunday_schedule_from_minutes'], $_POST['sunday_schedule_to_hours'], $_POST['sunday_schedule_to_minutes'] );
        //$provider->rate_per_minute = $rate_per_minute;
        
        $provider->rate_per_hour = $rate_per_hour;
        $provider->rate_first_hour = $rate_first_hour;
        $provider->rate_per_day = $rate_per_day;
        $provider->rate_first_day = $rate_first_day;
        //$provider->need_booking = $booking;
        $provider->public = $public;
        $provider->full_day = $full_day;

        $msg = 'added';
        if ($_POST['id'] > 0) {
            $provider->id = $_POST['id'];
            $provider->longitude = $longitude;
            $provider->latitude = $latitude;
            $currentProvider = Provider::find_by_id($_POST['id']);
            $provider->status = $currentProvider->status;
            $msg = 'updated';

        }
        else {
            $provider->status = Provider::STATUS_ACTIVE;
            //add lat & long
            $location = $address .', '.$city.', '.$postcode;
            $url = 'http://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($location);
            $data = json_decode(file_get_contents($url), true);

            $lat = 0;
            $long = 0;
            if($data['status']='OK'){
                $latLong =  $data['results'][0]['geometry']['location'];
                $lat = $latLong['lat'];
                $long = $latLong['lng'];
            }
            $provider->longitude = $long;
            $provider->latitude = $lat;
        }

        $provider->save();
        global $SPOT_IMAGES_PATH;
        global $ALLOWED_IMAGE_TYPES;
        if(!empty($_FILES['images'])){
            $images = $_FILES['images'];
            foreach($_FILES['images']["error"] as $key=>$uploaderror){
                if ($uploaderror == UPLOAD_ERR_OK){
                    $imgNameParts = explode('.', $images['name'][$key]);
                    $imgExt = $imgNameParts[count($imgNameParts) - 1];
                    if(in_array($imgExt, $ALLOWED_IMAGE_TYPES)){
                        $imageName = uniqid() . str_replace(' ','', $imgNameParts[0]) .'.'. $imgExt;
                        $imagePath = $SPOT_IMAGES_PATH.$imageName;
                        if(move_uploaded_file($images['tmp_name'][$key], $imagePath)){
                            Provider::saveImageForProvider($imageName, $provider->id);
                        }else{
                            
                        }
                    }
                    else{
                        
                    }
                }
            }
        }
        // Success
        $session->message("Provider ". $msg ." successfully.");
        redirect_to('dashboard.php');
    }

}
else {
    if ($id_provider > 0) {
        $providerO             = Provider::getInstance();
        $provider              = $providerO->find_by_id($id_provider);
        $first_name = $provider->first_name ;
        $last_name = $provider->last_name;
        $phone = $provider->phone;
        $email = $provider->email;
        $address = $provider->address;
        $city = $provider->city;
        $postcode = $provider->postcode;
        $longitude = $provider->longitude;
        $latitude = $provider->latitude;
        $access_information = $provider->access_information ;
        /*$monday_schedule = $provider->monday_schedule;
        $tuesday_schedule = $provider->tuesday_schedule;
        $wednesday_schedule = $provider->wednesday_schedule ;
        $thursday_schedule = $provider->thursday_schedule;
        $friday_schedule = $provider->friday_schedule;
        $saturday_schedule = $provider->saturday_schedule;
        $sunday_schedule = $provider->sunday_schedule;*/
        $monday_schedule = prepare_schedule_for_select($provider->monday_schedule);
        $monday_from_hours = $monday_schedule[0];
        $monday_from_minutes = $monday_schedule[1];
        $monday_to_hours = $monday_schedule[2];
        $monday_to_minutes = $monday_schedule[3];
        $tuesday_schedule = prepare_schedule_for_select($provider->tuesday_schedule);
        $tuesday_from_hours = $tuesday_schedule[0];
        $tuesday_from_minutes = $tuesday_schedule[1];
        $tuesday_to_hours = $tuesday_schedule[2];
        $tuesday_to_minutes = $tuesday_schedule[3];
        $wednesday_schedule = prepare_schedule_for_select($provider->wednesday_schedule);
        $wednesday_from_hours = $wednesday_schedule[0];
        $wednesday_from_minutes = $wednesday_schedule[1];
        $wednesday_to_hours = $wednesday_schedule[2];
        $wednesday_to_minutes = $wednesday_schedule[3];
        $thursday_schedule = prepare_schedule_for_select($provider->thursday_schedule);
        $thursday_from_hours = $thursday_schedule[0];
        $thursday_from_minutes = $thursday_schedule[1];
        $thursday_to_hours = $thursday_schedule[2];
        $thursday_to_minutes = $thursday_schedule[3];
        $friday_schedule = prepare_schedule_for_select($provider->friday_schedule);
        $friday_from_hours = $friday_schedule[0];
        $friday_from_minutes = $friday_schedule[1];
        $friday_to_hours = $friday_schedule[2];
        $friday_to_minutes = $friday_schedule[3];
        $saturday_schedule = prepare_schedule_for_select($provider->saturday_schedule);
        $saturday_from_hours = $saturday_schedule[0];
        $saturday_from_minutes = $saturday_schedule[1];
        $saturday_to_hours = $saturday_schedule[2];
        $saturday_to_minutes = $saturday_schedule[3];
        $sunday_schedule = prepare_schedule_for_select($provider->sunday_schedule);
        $sunday_from_hours = $sunday_schedule[0];
        $sunday_from_minutes = $sunday_schedule[1];
        $sunday_to_hours = $sunday_schedule[2];
        $sunday_to_minutes = $sunday_schedule[3];
        //$rate_per_minute = $provider->rate_per_minute;
        $rate_per_hour     = $provider->rate_per_hour;
        $rate_first_hour   = $provider->rate_first_hour;
        $rate_per_day     = $provider->rate_per_day;
        $rate_first_day   = $provider->rate_first_day;
        //$booking = $provider->need_booking;
        $full_day = $provider->full_day;
        $public = $provider->public;
    }
}
?>

<?php render_layout_template('admin_header.php', array('session' => $session, 'user' => $user, 'is_dashboard_page'=>true)); ?>

<!-- Container -->
<div id="container">
<div class="shell">

<!-- Small Nav -->
<div class="small-nav">
    <a href="#">Dashboard</a>
    <span>&gt;</span>
    Current Providers
</div>
<!-- End Small Nav -->


<br/>
<!-- Main -->
<div id="main">
<div class="cl">&nbsp;</div>

<!-- Content -->
<div id="content">

    <!-- Box -->
    <div class="box">
        <!-- Box Head -->
        <div class="box-head">
            <h2 class="left">Current Providers</h2>
        </div>
        <!-- End Box Head -->

        <?php if ( strlen($session->message())): ?>
            <!-- Message OK -->
            <div class="msg msg-ok">
                <p><strong><?php echo $session->message(); ?></strong></p>
            </div>
            <!-- End Message OK -->
        <?php endif; ?>
        <?php
            $providerO          = Provider::getInstance();
            $providers = $providerO->find_all();
        ?>
        <?php if (!empty($providers)) : ?>
            <!-- Table -->
            <div class="table">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Postcode</th>
                        <th>City</th>
                        <th>Phone#</th>
                        <th>Status</th>
                        <th width="110" class="ac">Content Control</th>
                    </tr>

                    <?php foreach ($providers as $k => $provider) : ?>

                        <tr <?php echo ($k%2 != 0) ? 'class="odd"' : ''; ?> >
                            <td><h3><a href="view_provider.php?id=<?php echo $provider->id; ?>"><?php echo $provider->first_name?></a></h3></td>
                            <td><h3><a href="view_provider.php?id=<?php echo $provider->id; ?>"><?php echo $provider->last_name; ?></a></h3></td>
                            <td><h3><?php echo $provider->address ?></h3></td>
                            <td><h3><?php echo $provider->postcode; ?></h3></td>
                            <td><h3><?php echo $provider->city?></h3></td>
                            <td><h3><?php echo $provider->phone?></h3></td>
                            <?php $new_status = ($provider->status == Provider::STATUS_ACTIVE) ? Provider::STATUS_INACTIVE :Provider::STATUS_ACTIVE;  ?>
                            <td><?php echo ucfirst($provider->status); ?>&nbsp; <a href="change_status.php?id=<?php echo $provider->id; ?>">Set as <?php echo ucfirst($new_status); ?></a></td>
                            <td><a  href="spot_bookings.php?providerid=<?php echo $provider->id;?>">Current Bookings</a><a href="dashboard.php?id=<?php echo $provider->id; ?>#form-provider" class="ico edit">Edit</a>&nbsp;&nbsp;<a href="delete_provider.php?id=<?php echo $provider->id; ?>" class="ico del" data-confirm="Are you sure you want to delete this provider?">Delete</a></td>
                            <!--  -->
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <!-- Table -->
        <?php else : ?>
            <p>There are no providers at the moment!</p>
        <?php endif; ?>

    </div>
    <!-- End Box -->

    <!-- Box -->
    <div class="box">
        <!-- Box Head -->
        <div class="box-head">
            <h2><?php echo $add_edit_msg;?> Provider</h2>
        </div>
        <!-- End Box Head -->



        <?php if (!empty($errors)): ?>
            <ul class="form-errors">
                <?php foreach ($errors as $error) : ?>
                    <?php echo $error; ?>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>

        <form action="<?php echo $_SERVER['PHP_SELF'] . ($id_provider> 0 ? '?id='.$id_provider :''); ?>" method="post" id="form-provider">

            <!-- Form -->
            <div class="form">
                <p><input type="hidden" name="id" value="<?php echo $id_provider;?>" /></p>

				<p class="clear"></p>
                <p class="inline-field">
                    <div class="left margin-right" >
                        <label>First Name</label>
                        <input type="text" name="first_name" class="field size4" value="<?php echo isset($first_name) ? $first_name: '' ?>" />
                    </div>

                    <div class="left margin-right">
                        <label>Last Name</label>
                        <input type="text" name="last_name" class="field size4" value="<?php echo isset($last_name) ? $last_name: '' ?>" />
                    </div>
                </p>
                <p class="clear"></p>

                <p class="inline-field">
                    <div class="left margin-right" >
                        <label>Phone</label>
                        <input type="text" name="phone" class="field size4" value="<?php echo isset($phone) ? $phone: '' ?>" />
                    </div>
                    <div class="left margin-right" >
                        <label>Email</label>
                        <input type="text" name="email" class="field size4" value="<?php echo isset($email) ? $email: '' ?>" />
                    </div>
                </p>
                <p class="clear"></p>
                <p class="inline-field">
                    <div class="left margin-right" >
                        <label>Address</label>
                        <input type="text" name="address" class="field size4" value="<?php echo isset($address) ? $address: '' ?>" />
                    </div>
                    <div class="left margin-right" >
                        <label>City</label>
                        <input type="text" name="city" class="field size4" value="<?php echo isset($city) ? $city: '' ?>" />
                    </div>
                    <div class="left" >
                        <label>Postcode</label>
                        <input type="text" name="postcode" class="field size4" value="<?php echo isset($postcode) ? $postcode: '' ?>" />
                    </div>
                </p>
                <p class="clear"></p>

                <?php if ($id_provider >0) : ?>
                <p>
                    <div class="left margin-right">
                        <label>Latitude</label>
                        <input type="text" name="latitude" class="field size4" value="<?php echo isset($latitude) ? $latitude: '' ?>" />
                    </div>

                    <div class="left ">
                        <label>Longitude</label>
                        <input type="text" name="longitude" class="field size4" value="<?php echo isset($longitude) ? $longitude: '' ?>" />
                    </div>

                </p>
                <p class="clear"></p>
                <?php endif; ?>


                <br>
                <p>
                    <label>Access Information</label>
                    <textarea rows="3" cols="10" name="access_information" class="field size5"><?php echo isset($access_information) ? $access_information: '' ?></textarea>
                </p>

                <p>
                    <div >
                        <label for="monday_include">Monday Schedule <?php echo $additional_info; ?> </label>
                        <input type="checkbox" id="monday_include">
                        <div class="left margin-right">
                            <p><strong>From</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="monday_schedule_from_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>" <?php echo (isset($monday_from_hours) && $monday_from_hours == $hour) ?  'selected="selected"' : ''  ?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="monday_schedule_from_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($monday_from_minutes) && $monday_from_minutes == $minute) ? 'selected="selected"' : '' ?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                        <div class="left margin-right">
                            <p><strong>To</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="monday_schedule_to_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($monday_to_hours) && $monday_to_hours == $hour) ? 'selected="selected"' : ''?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="monday_schedule_to_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($monday_to_minutes) && $monday_to_minutes == $minute) ?  'selected="selected"' : ''?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>
                        <?php /*<input type="text" name="monday_schedule" class="field size4" value="<?php echo isset($monday_schedule) ? $monday_schedule: '' ?>" />*/ ?>
                    </div>


                </p>
                <p class="clear"></p>
                <p>
                    <div >
                        <label for="tuesday_include">Tuesday Schedule <?php echo $additional_info; ?> </label>
                        <input type="checkbox" id="tuesday_include">
                        <div class="left margin-right">
                            <p><strong>From</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="tuesday_schedule_from_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($tuesday_from_hours) && $tuesday_from_hours == $hour) ?  'selected="selected"' : ''?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="tuesday_schedule_from_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($tuesday_from_minutes) && $tuesday_from_minutes == $minute) ? 'selected="selected"' : ''?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                        <div class="left margin-right">
                            <p><strong>To</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="tuesday_schedule_to_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($tuesday_to_hours) && $tuesday_to_hours == $hour) ? 'selected="selected"' : '' ?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="tuesday_schedule_to_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($tuesday_to_minutes) && $tuesday_to_minutes == $minute) ? 'selected="selected"' : ''?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                    </div>


                </p>
                <p class="clear"></p>
                <p>
                    <div >
                        <label for="wednesday_include">Wednesday Schedule <?php echo $additional_info; ?> </label>
                        <input type="checkbox" id="wednesday_include">
                        <div class="left margin-right">
                            <p><strong>From</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="wednesday_schedule_from_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($wednesday_from_hours) && $wednesday_from_hours == $hour) ?  'selected="selected"' : ''?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="wednesday_schedule_from_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($wednesday_from_minutes) && $wednesday_from_minutes == $minute) ? 'selected="selected"' : ''?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                        <div class="left margin-right">
                            <p><strong>To</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="wednesday_schedule_to_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($wednesday_to_hours) && $wednesday_to_hours == $hour) ?  'selected="selected"' : ''?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="wednesday_schedule_to_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($wednesday_to_minutes) && $wednesday_to_minutes == $minute) ? 'selected="selected"' : ''?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                    </div>


                </p>
                <p class="clear"></p>
                <p>
                    <div >
                        <label for="thursday_include">Thursday Schedule <?php echo $additional_info; ?> </label>
                        <input type="checkbox" id="thursday_include">
                        <div class="left margin-right">
                            <p><strong>From</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="thursday_schedule_from_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($thursday_from_hours) && $thursday_from_hours == $hour) ?  'selected="selected"' : '' ?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="thursday_schedule_from_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($thursday_from_minutes) && $thursday_from_minutes == $minute) ?  'selected="selected"' : '' ?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                        <div class="left margin-right">
                            <p><strong>To</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="thursday_schedule_to_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($thursday_to_hours) && $thursday_to_hours == $hour) ?  'selected="selected"' : ''?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="thursday_schedule_to_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($thursday_to_minutes) && $thursday_to_minutes == $minute) ? 'selected="selected"' : ''?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                    </div>


                </p>
                <p class="clear"></p>
                <p>
                    <div >
                        <label for="friday_include">Friday Schedule <?php echo $additional_info; ?> </label>
                        <input type="checkbox" id="friday_include">
                        <div class="left margin-right">
                            <p><strong>From</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="friday_schedule_from_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($friday_from_hours) && $friday_from_hours == $hour) ? 'selected="selected"' : ''?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="friday_schedule_from_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($friday_from_minutes) && $friday_from_minutes == $minute) ?  'selected="selected"' : ''?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                        <div class="left margin-right">
                            <p><strong>To</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="friday_schedule_to_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($friday_to_hours) && $friday_to_hours == $hour) ? 'selected="selected"' : ''?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="friday_schedule_to_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($friday_to_minutes) && $friday_to_minutes == $minute) ? 'selected="selected"' : ''?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                    </div>


                </p>
                <p class="clear"></p>
                <p>
                    <div >
                        <label for="saturday_include">Saturday Schedule <?php echo $additional_info; ?> </label>
                        <input type="checkbox" id="saturday_include">
                        <div class="left margin-right">
                            <p><strong>From</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="saturday_schedule_from_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($saturday_from_hours) && $saturday_from_hours == $hour) ? 'selected="selected"' : ''?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="saturday_schedule_from_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($saturday_from_minutes) && $saturday_from_minutes == $minute) ? 'selected="selected"' : ''?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                        <div class="left margin-right">
                            <p><strong>To</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="saturday_schedule_to_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($saturday_to_hours) && $saturday_to_hours == $hour) ? 'selected="selected"' : ''?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="saturday_schedule_to_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($saturday_to_minutes) && $saturday_to_minutes == $minute) ?  'selected="selected"' : ''?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                    </div>


                </p>
                <p class="clear"></p>
                <p>
                    <div >
                        <label for="sunday_include">Sunday Schedule <?php echo $additional_info; ?> </label>
                        <input type="checkbox" id="sunday_include">
                        <div class="left margin-right">
                            <p><strong>From</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="sunday_schedule_from_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($sunday_from_hours) && $sunday_from_hours == $hour) ?  'selected="selected"' : ''?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="sunday_schedule_from_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($sunday_from_minutes) && $sunday_from_minutes == $minute) ? 'selected="selected"' : ''?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                        <div class="left margin-right">
                            <p><strong>To</strong></p>
                            <div  class="left margin-right">
                                <span>Hours</span>
                                <select name="sunday_schedule_to_hours" class="field size2" >
                                    <?php foreach ($hours as $hour): ?>
                                        <option value="<?php echo $hour; ?>"  <?php echo (isset($sunday_to_hours) && $sunday_to_hours == $hour) ?  'selected="selected"' : ''?>><?php echo $hour; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div  class="left margin-right">
                                <span>Minutes</span>
                                <select name="sunday_schedule_to_minutes" class="field size2" >
                                    <?php foreach ($minutes as $minute): ?>
                                        <option value="<?php echo $minute; ?>"  <?php echo (isset($sunday_to_minutes) && $sunday_to_minutes == $minute) ? 'selected="selected"' : ''?>><?php echo $minute; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p class="clear"></p>
                        </div>

                    </div>


                </p>

                <?php /*
                <p>
                    <div class="left margin-right">
                        <label>Tuesday Schedule <?php echo $additional_info; ?></label>
                        <input type="text" name="tuesday_schedule" class="field size4" value="<?php echo isset($tuesday_schedule) ? $tuesday_schedule: '' ?>" />
                    </div>

                    <div class="left ">
                        <label>Wednesday Schedule <?php echo $additional_info; ?></label>
                        <input type="text" name="wednesday_schedule" class="field size4" value="<?php echo isset($wednesday_schedule) ? $wednesday_schedule: '' ?>" />
                    </div>
                    <div class="left margin-right">
                        <label>Thursday Schedule <?php echo $additional_info; ?></label>
                        <input type="text" name="thursday_schedule" class="field size4" value="<?php echo isset($thursday_schedule) ? $thursday_schedule: '' ?>" />
                    </div>
                    <div class="left margin-right">
                        <label>Friday Schedule <?php echo $additional_info; ?></label>
                        <input type="text" name="friday_schedule" class="field size4" value="<?php echo isset($friday_schedule) ? $friday_schedule: '' ?>" />
                    </div>

                    <div class="left ">
                        <label>Saturday Schedule <?php echo $additional_info; ?></label>
                        <input type="text" name="saturday_schedule" class="field size4" value="<?php echo isset($saturday_schedule) ? $saturday_schedule: '' ?>" />
                    </div>

                </p>
                <<p class="clear"></p>
                <p>
                    <div class="left margin-right">
                        <label>Sunday Schedule <?php echo $additional_info; ?></label>
                        <input type="text" name="sunday_schedule" class="field size4" value="<?php echo isset($sunday_schedule) ? $sunday_schedule: '' ?>" />
                    </div>


                </p>*/ ?>
                <p class="clear"></p>
                <p class="clear"></p>
                <p>
                    <div class="left margin-right">
                        <label>Full day (Longterm)</label>
                        <select id="full_day" name="full_day" class="size4" onchange="selectLongTerm(this.value);">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
           
                    </div>
                </p>
                <p class="clear"></p>
                <div class="short_term">
                <p>
                    <div class="left margin-right">
                        <label>Rate for first hour ($)</label>
                        <input type="text" name="rate_first_hour" class="field size4" value="<?php echo isset($rate_first_hour) ? $rate_first_hour: '' ?>" >
                    </div>
                    <div class="left margin-right">
                        <label>Rate per hour ($)</label>
                        <input type="text" name="rate_per_hour" class="field size4" value="<?php echo isset($rate_per_hour) ? $rate_per_hour: '' ?>" >
                    </div>
                </p>
                </div>
                <div class="long_term">
                <p>
                    <div class="left margin-right">
                        <label>Rate for first day ($)</label>
                        <input type="text" name="rate_first_day" class="field size4" value="<?php echo isset($rate_first_day) ? $rate_first_day: '' ?>" >
                    </div>
                    <div class="left margin-right">
                        <label>Rate per day ($)</label>
                        <input type="text" name="rate_per_day" class="field size4" value="<?php echo isset($rate_per_day) ? $rate_per_day: '' ?>" >
                    </div>
                </p>
                </div>
                <p class="clear"></p>
                <p>
                    <div class="left margin-right">
                        <label>Public</label>
                        <select name="public" class="size4">
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>     
                    </div>
                </p>
                <p class="clear"></p>
                <p>
                    <div class="left margin-right">
                        <label>Select images to upload</label>
                        <input type="file" name="images[]" class="field size4" value="" >
                        <span class="cursor-pntr" id="addimage">Add an other image</span>
                    </div>
                 </p>
                 <p class="clear"></p>
                <br id="imgclear">
                <br class="clear">
                <br class="clear">
            </div>
            <!-- End Form -->

            <!-- Form Buttons -->
            <div class="buttons">

                <input type="submit" class="button" value="submit"/>
            </div>
            <!-- End Form Buttons -->
        </form>
    </div>
    <!-- End Box -->

</div>
<!-- End Content -->

<!-- Sidebar -->
<div id="sidebar">

    <!-- Box -->
    <div class="box">

        <!-- Box Head -->
        <div class="box-head">
            <h2>Management</h2>
        </div>
        <!-- End Box Head-->

        <div class="box-content">
            <a href="dashboard.php#form-provider" class="add-button"><span>Add new Provider</span></a>

            <div class="cl">&nbsp;</div>
        </div>
    </div>
    <!-- End Box -->
</div>
<!-- End Sidebar -->

<div class="cl">&nbsp;</div>
</div>
<!-- Main -->
</div>
</div>
<!-- End Container -->

<script type="text/javascript">
    $(document).ready(function(){
        $('a.del').on('click', function(e){
            e.preventDefault();

            if(confirm($(this).attr('data-confirm'))) {
                window.location = $(this).attr('href');
            }

            return false;
        });
        $('.tooltip').tooltip();
        $('#addimage').click(function(){
            $(this).before('<input type="file" name="images[]" class="field size4" value="" >');

        });
        initialize();
    });
    function initialize()
    {
        weekdays = Array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');
        for(var i=0; i < weekdays.length; i++)
        {
            $("#"+weekdays[i]+"_include").change(function(){includeExcludeDay(this)});
            
        }
    }
    function includeExcludeDay(ele)
    {
        day = ele.id.split('_')[0];
        if($(ele).is(":checked")){
            $('select[name='+day +'_schedule_from_hours').val('00').prop('disabled', false);
            $('select[name='+day +'_schedule_from_minutes').val('00').prop('disabled', false);
            $('select[name='+day +'_schedule_to_hours]').val('23').prop('disabled', false);
            $('select[name='+day +'_schedule_to_minutes').val('45').prop('disabled', false);
            
        }else{
            $('select[name='+day +'_schedule_from_hours').val('00').prop('disabled', true);
            $('select[name='+day +'_schedule_from_minutes').val('00').prop('disabled', true);
            $('select[name='+day +'_schedule_to_hours]').val('00').prop('disabled', true);
            $('select[name='+day +'_schedule_to_minutes').val('00').prop('disabled', true);
        }
        
    }
    function selectLongTerm(val)
    {
        if(val == 'No'){
            $('.short_term').show();
            $('.long_term').hide();
        }else{
            $('.short_term').hide();
            $('.long_term').show(); 
        }
    }
    selectLongTerm('<?php echo $full_day;?>');
    $('#full_day').val('<?php echo $full_day;?>');
</script>

<?php include_layout_template('admin_footer.php'); ?>

