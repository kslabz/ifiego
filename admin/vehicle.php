<?php
require_once('../includes/initialize.php');
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}

$id_user = isset($_GET['userid']) ? $_GET['userid'] : 0;
$id_vhcl = isset($_GET['id']) ? $_GET['id'] : 0;
$msg = isset($_GET['msg']) ? $_GET['msg'] : '';

$add_edit_msg = "Edit";
$userO = User::getInstance();
$user = $userO->find_by_id($session->user_id);
$userViewed = $userO->find_by_id($id_user);

//$additional_info = '<a href="#" class="tooltip" title="'.$schedule_format.'"><img src="'.rtrim($base_url,'/').'/media/admin/images/info-icon.png" alt="" /></a>';
$pattern_schedule = "/^([0-9][0-9]:[0-9][0-9]-[0-9][0-9]:[0-9][0-9])$/";
if (!strcmp($_SERVER['REQUEST_METHOD'],'POST')) {
    $registraion         = trim(strip_tags($_POST['registration']));
    $type            = trim(strip_tags($_POST['type']));

    $errors = array();
    if ($type === ''){
        $errors[] = '<li>Vehicle Type is a required field</li>';
    }
    if ($registraion === ''){
        $errors[] = '<li>Registraion # is a required field</li>';
    }
    
    if (empty($errors)){
        User::saveVehicle($id_user, $id_vhcl, $registraion, $type);
        $session->message("User Vehicle ". $msg ." successfully.");
        redirect_to('user.php?userid='.$id_user);
    }
    
}
else {
    if ($id_vhcl > 0) {
        $vehicle = User::getVehicle($id_vhcl);
        //print_r($vehicle[0]);
        $registration = $vehicle[0]['registration'] ;
        $type = $vehicle[0]['type'];
    }
}
?>

<?php render_layout_template('admin_header.php', array('session' => $session, 'user' => $user, 'is_user_page'=>true)); ?>

<!-- Container -->
<div id="container">
<div class="shell">

<!-- Small Nav -->
<div class="small-nav">
    <a href="user.php">User</a>
    <span>&gt;</span>
    Vehicles
</div>
<!-- End Small Nav -->


<br/>
<!-- Main -->
<div id="main">
<div class="cl">&nbsp;</div>

<!-- Content -->
<div id="content">

    <!-- Box -->
    <div class="box">
        <!-- Box Head -->
        <div class="box-head">
            <h2 class="left"><?php echo $userViewed->first_name . ' ' .$userViewed->last_name;?>'s Vehicles </h2>
        </div>
        <!-- End Box Head -->

        <?php if ( strlen($session->message())): ?>
            <!-- Message OK -->
            <div class="msg msg-ok">
                <p><strong><?php echo $session->message(); ?></strong></p>
            </div>
            <!-- End Message OK -->
        <?php endif; ?>
        <?php

            $vehicles = User::getUserVehicles($id_user);
        ?>
        <?php if (!empty($vehicles)) : 
            global $VEHICLE_IMAGES_BASE_URL;
            ?>
            <!-- Table -->
            <div class="table">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        
                        <th>Registration  </th>
                        <th> Type</th>
                        <th>Registration Papers</th>         
                        <th width="" class="ac">Content Control</th>
                    </tr>

                    <?php foreach ($vehicles as $k => $v) : ?>

                        <tr <?php echo ($k%2 != 0) ? 'class="odd"' : ''; ?> >
                            <td><h3><?php echo $v['registration'] ?></h3></td>
                            <td><h3><?php echo $v['type'] ?></h3></td>
                            <td><h3><?php if($v['image_path']){?><a href="<?php echo $VEHICLE_IMAGES_BASE_URL.$v['image_path'];?>" >
                                        <img class="img-thumb50" src="<?php echo $VEHICLE_IMAGES_BASE_URL.$v['image_path'];?>" /></a><?php } ?></h3></td>
                            <td class="ac">
                                <a href="vehicle.php?userid=<?php echo $id_user;?>&id=<?php echo $v['id']; ?>#form-vehicle" class="ico edit">Edit</a>&nbsp;&nbsp;
                                <a href="delete_vehicle.php?id=<?php echo $v['id']; ?>" class="ico del" data-confirm="Are you sure you want to delete this Vehicle?">Delete</a>
                            </td>
                            <!--  -->
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <!-- Table -->
        <?php else : ?>
            <p>There are no Vehicles at the moment!</p>
        <?php endif; ?>

    </div>
    <!-- End Box -->

    <!-- Box -->
    <div class="box">
        <!-- Box Head -->
        <div class="box-head">
            <h2><?php echo $add_edit_msg;?> Provider</h2>
        </div>
        <!-- End Box Head -->



        <?php if (!empty($errors)): ?>
            <ul class="form-errors">
                <?php foreach ($errors as $error) : ?>
                    <?php echo $error; ?>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>

        <form action="<?php echo $_SERVER['PHP_SELF'] . ($id_user> 0 ? '?userid='.$id_user.'&id='.$id_vhcl :''); ?>" method="post" id="form-vehicle">

            <!-- Form -->
            <div class="form">
                <p><input type="hidden" name="id" value="<?php echo $id_vhcl;?>" /></p>

				<p class="clear"></p>
                <p class="inline-field">
                    <div class="left margin-right" >
                        <label>Registration #</label>
                        <input type="text" name="registration" class="field size4" value="<?php echo isset($registration) ? $registration: '' ?>" />
                    </div>

                    <div class="left margin-right">
                        <label>type</label>
                        <input type="text" name="type" class="field size4" value="<?php echo isset($type) ? $type: '' ?>" />
                    </div>
                </p>
                
                
                
                <p class="clear"></p>
            </div>
            <!-- End Form -->

            <!-- Form Buttons -->
            <div class="buttons">
                <input type="submit" class="button" value="submit"/>
            </div>
            <!-- End Form Buttons -->
        </form>
    </div>
    <!-- End Box -->

</div>
<!-- End Content -->



<div class="cl">&nbsp;</div>
</div>
<!-- Main -->
</div>
</div>
<!-- End Container -->

<script type="text/javascript">
    $(document).ready(function(){
        $('a.del').on('click', function(e){
            e.preventDefault();
            if(confirm($(this).attr('data-confirm'))) {
                window.location = $(this).attr('href');
            }

            //return false;
        });

        $('.tooltip').tooltip();
     });
</script>
<?php include_layout_template('admin_footer.php'); ?>

