<?php
require_once('../includes/initialize.php');
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}
$userO = User::getInstance();
$user = $userO->find_by_id($session->user_id);
$id_user = isset($_GET['id']) ? $_GET['id'] : 0;
$msg = isset($_GET['msg']) ? $_GET['msg'] : '';

$add_edit_msg = "Add New";
if ($id_user > 0) {
    $add_edit_msg = "Edit";
}

$hours = get_hours();


//$schedule_format = "Use this format: hh:mm-hh:mm<br>For not available use 00:00-00:00";
$schedule_format = "For not available leave 00 everywhere";
$pattern_schedule = "/^([0-9][0-9]:[0-9][0-9]-[0-9][0-9]:[0-9][0-9])$/";
if (!strcmp($_SERVER['REQUEST_METHOD'],'POST')) {
    $first_name         = trim(strip_tags($_POST['first_name']));
    $last_name          = trim(strip_tags($_POST['last_name']));
    $phone              = trim(strip_tags($_POST['phone']));
    $email              = trim(strip_tags($_POST['email']));
    //$type            = trim(strip_tags($_POST['type']));
    $status            = trim(strip_tags($_POST['status']));
    $password            = $_POST['password'];
    if ($_POST['id'] > 0) {
        
    }

    $errors = array();
    if ($first_name === ''){
        $errors[] = '<li>First Name is a required field</li>';
    }
    if ($last_name === ''){
        $errors[] = '<li>Last Name is a required field</li>';
    }
    if ($phone === ''){
        $errors[] = '<li>Phone is a required field</li>';
    }
    if ($email === ''){
        $errors[] = '<li>Email is a required field</li>';
    } elseif (strlen($email)){
        $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/";
        if (!(preg_match($pattern, $email))) {
            $errors[] = '<li>Email is not a valid email address</li>';
        }
    }
//    if ($type === ''){
//        $errors[] = '<li>User type is a required field</li>';
//    }
    if ($status === ''){
        $errors[] = '<li>User status is a required field</li>';
    }
    if(empty($password) && $_POST['id'] == 0){
        $errors[] = '<li>Password is a required field</li>';
    }
    if (empty($errors)){
        $user          = User::getInstance();

        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->phone = $phone;
        $user->email = $email;
        $user->username = $email;
        $user->type = 'user';
        $user->status = $status;
        $msg = 'added';
        if ($_POST['id'] > 0) {
            $user->id = $_POST['id'];
            //$user->longitude = $longitude;
            //$user->latitude = $latitude;
            $currentUser = User::find_by_id($_POST['id']);
            if(empty($_POST['password'])){
                $user->password = $currentUser->password;
            }
            $msg = 'updated';
        }
        else {
            
        }
        if(!empty($password)){
            $user->password = encryptPassword($password);
        }
        $user->save();
        // Success
        $session->message("User ". $msg ." successfully.");
        redirect_to('user.php#');
    }

}
else {
    if ($id_user > 0) {
        $userO             = User::getInstance();
        $user              = $userO->find_by_id($id_user);
        $first_name = $user->first_name ;
        $last_name = $user->last_name;
        $phone = $user->phone;
        $email = $user->email;
        //$type = $user->type;
        $status = $user->status;
    }
}
?>

<?php render_layout_template('admin_header.php', array('session' => $session, 'user' => $user, 'is_user_page'=>true)); ?>

<!-- Container -->
<div id="container">
<div class="shell">

<!-- Small Nav -->
<div class="small-nav">
    <a href="#">Dashboard</a>
    <span>&gt;</span>
    Current Users
</div>
<!-- End Small Nav -->


<br/>
<!-- Main -->
<div id="main">
<div class="cl">&nbsp;</div>

<!-- Content -->
<div id="content">

    <!-- Box -->
    <div class="box">
        <!-- Box Head -->
        <div class="box-head">
            <h2 class="left">Current Users</h2>
        </div>
        <!-- End Box Head -->

        <?php if ( strlen($session->message())): ?>
            <!-- Message OK -->
            <div class="msg msg-ok">
                <p><strong><?php echo $session->message(); ?></strong></p>
            </div>
            <!-- End Message OK -->
        <?php endif; ?>
        <?php
            $userO          = User::getInstance();
            $users = $userO->getAllStandardUsers();
        ?>
        <?php if (!empty($users)) : ?>
            <!-- Table -->
            <div class="table">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <th>Full Name </th>
                        <th>Email</th>
                        
                        <th>Status</th>                        
                        <th width="" class="ac">Content Control</th>
                    </tr>

                    <?php foreach ($users as $k => $userv) : ?>

                        <tr <?php echo ($k%2 != 0) ? 'class="odd"' : ''; ?> >
                            <td><h3><a href="view_user.php?id=<?php echo $userv->id; ?>"><?php echo $userv->first_name.' '.$userv->last_name; ?></a></h3></td>
                            <td><h3><?php echo $userv->email ?></h3></td>
                            
                            
                            <td><?php echo ucfirst($userv->status); ?>&nbsp; </td>
                            <td><a href="vehicle.php?userid=<?php echo $userv->id; ?>" class="">Vehicles</a>&nbsp;
                                <a href="user_booking.php?userid=<?php echo $userv->id; ?>" class="">Bookings</a>
                                <a href="user.php?id=<?php echo $userv->id; ?>#form-user" class="ico edit">Edit</a>&nbsp;&nbsp;
                                <a href="delete_user.php?id=<?php echo $userv->id; ?>" class="ico del" data-confirm="Are you sure you want to delete this user?">Delete</a></td>
                            <!--  -->
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <!-- Table -->
        <?php else : ?>
            <p>There are no users at the moment!</p>
        <?php endif; ?>

    </div>
    <!-- End Box -->

    <!-- Box -->
    <div class="box">
        <!-- Box Head -->
        <div class="box-head">
            <h2><?php echo $add_edit_msg;?> User</h2>
        </div>
        <!-- End Box Head -->



        <?php if (!empty($errors)): ?>
            <ul class="form-errors">
                <?php foreach ($errors as $error) : ?>
                    <?php echo $error; ?>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>

        <form action="<?php echo $_SERVER['PHP_SELF'] . ($id_user> 0 ? '?id='.$id_user :''); ?>#form-user" method="post" id="form-user">

            <!-- Form -->
            <div class="form">
                <p><input type="hidden" name="id" value="<?php echo $id_user;?>" /></p>

				<p class="clear"></p>
                <p class="inline-field">
                    <div class="left margin-right" >
                        <label>First Name</label>
                        <input type="text" name="first_name" class="field size4" value="<?php echo isset($first_name) ? $first_name: '' ?>" />
                    </div>

                    <div class="left margin-right">
                        <label>Last Name</label>
                        <input type="text" name="last_name" class="field size4" value="<?php echo isset($last_name) ? $last_name: '' ?>" />
                    </div>
                </p>
                <p class="clear"></p>

                <p class="inline-field">
                    <div class="left margin-right" >
                        <label>Phone</label>
                        <input type="text" name="phone" class="field size4" value="<?php echo isset($phone) ? $phone: '' ?>" />
                    </div>
                    <div class="left margin-right" >
                        <label>Email</label>
                        <input type="text" name="email" class="field size4" value="<?php echo isset($email) ? $email: '' ?>" />
                    </div>
                </p>
                <p class="clear"></p>
                <p class="inline-field">
                    <div class="left margin-right" >
                        <label>Password</label>
                        <input type="password" name="password" class="field size4" placeholder="Leave Empty for No Change" />
                    </div>
                </p>
                <p class="clear"></p>
                <p class="inline-field">

                    <div class="left margin-right" >
                        <label>Status</label>
                        <select id="user_status" name="status">                            
                            <option value="active">Active</option>
                            <option value="inactive">Inactive</option>
                            <option value="blocked">Blocked</option>
                        </select>
                    </div>
                </p>
                <p class="clear"></p>
            </div>
            <!-- End Form -->

            <!-- Form Buttons -->
            <div class="buttons">
                <input type="submit" class="button" value="submit"/>
            </div>
            <!-- End Form Buttons -->
        </form>
    </div>
    <!-- End Box -->

</div>
<!-- End Content -->

<!-- Sidebar -->
<div id="sidebar">

    <!-- Box -->
    <div class="box">

        <!-- Box Head -->
        <div class="box-head">
            <h2>Management</h2>
        </div>
        <!-- End Box Head-->

        <div class="box-content">
            <a href="user.php#form-user" class="add-button"><span>Add new User</span></a>

            <div class="cl">&nbsp;</div>
        </div>
    </div>
    <!-- End Box -->
</div>
<!-- End Sidebar -->

<div class="cl">&nbsp;</div>
</div>
<!-- Main -->
</div>
</div>
<!-- End Container -->

<script type="text/javascript">
    $(document).ready(function(){
        $('a.del').on('click', function(e){
            e.preventDefault();

            if(confirm($(this).attr('data-confirm'))) {
                window.location = $(this).attr('href');
            }

            return false;
        });

        $('.tooltip').tooltip();
     });
     <?php if(!empty ($id_user)){ ?>
     
     $("#user_status").val('<?php echo $status;?>');
     <?php }?>
</script>

<?php include_layout_template('admin_footer.php'); ?>

