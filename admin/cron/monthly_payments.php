<?php
$includPath = $includPath = realpath( __DIR__ . '/../../includes/');
require_once ($includPath.'/initialize.php');
require_once ($includPath.'/PaypalHelper.php');
function getAmountForUsers(){
    $sql = "SELECT b.user_id, ROUND( SUM( b.price ) , 2 ) AS  'amount', ud.refresh_token, ud.correlation_id
            FROM bookings b
            JOIN user_paypal_details ud ON ud.user_id = b.user_id
            WHERE MONTH( b.timestamp ) = MONTH( NOW( ) ) 
            GROUP BY user_id";
    $prices = User::findBySql($sql);
    foreach ($prices as $pr){
        PaypalHelper::executePayment($pr['refresh_token'], $pr['correlation_id'], $pr['amount']);
    }
}
getAmountForUsers();