<?php require_once("../includes/initialize.php"); ?>
<?php if (!$session->is_logged_in()) {
    redirect_to("login.php");
} ?>
<?php
// must have an ID
if (empty($_GET['id'])) {
    $session->message("No provider ID was provided.");
    redirect_to('dashboard.php');
}

$provider = Provider::find_by_id($_GET['id']);
$new_status = ($provider->status == Provider::STATUS_ACTIVE) ? Provider::STATUS_INACTIVE :Provider::STATUS_ACTIVE;
$provider->status = $new_status;
$provider->save();

$session->message("The provider status has been changed.");
redirect_to('dashboard.php');


?>
<?php if (isset($database)) {
    $database->close_connection();
} ?>
