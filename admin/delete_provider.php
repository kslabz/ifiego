<?php require_once("../includes/initialize.php"); ?>
<?php if (!$session->is_logged_in()) {
    redirect_to("login.php");
} ?>
<?php
// must have an ID
if (empty($_GET['id'])) {
    $session->message("No provider ID was provided.");
    redirect_to('dashboard.php');
}

$provider = Provider::find_by_id($_GET['id']);
if ($provider && $provider->delete()) {
    $session->message("The provider {$provider->first_name} {$provider->last_name} was deleted.");
    redirect_to('dashboard.php');
} else {
    $session->message("The provider could not be deleted.");
    redirect_to('dashboard.php');
}

?>
<?php if (isset($database)) {
    $database->close_connection();
} ?>
