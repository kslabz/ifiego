<?php
require_once('../includes/initialize.php');
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}

//$id_user = isset($_GET['userid']) ? $_GET['userid'] : 0;
$provider_id = isset($_GET['id']) ? $_GET['id'] : 0;

$userO = User::getInstance();
$user = $userO->find_by_id($session->user_id);
$providerViewed = Provider::find_by_id($provider_id);
$images = Provider::getImagesByProvider($providerViewed->id);
?>

<?php render_layout_template('admin_header.php', array('session' => $session, 'user' => $user, 'is_dashboard_page'=>true)); ?>

<!-- Container -->
<div id="container">
<div class="shell">

<!-- Small Nav -->
<div class="small-nav">
    <a href="user.php">Providers</a>
    <span>&gt;</span>
    <?php echo $providerViewed->first_name . ' ' .$providerViewed->last_name;?>
</div>
<!-- End Small Nav -->


<br/>
<!-- Main -->
<div id="main">
<div class="cl">&nbsp;</div>

<!-- Content -->
<div id="content">

    <!-- Box -->
    <div class="box">
        <!-- Box Head -->
        <div class="box-head">
            <h2 class="left"><?php echo $providerViewed->first_name . ' ' .$providerViewed->last_name;?> </h2> <span class="right"><a class="ico edit" href="dashboard.php?id=<?php echo $providerViewed->id; ?>#form-provider">Edit</a></span>
        </div>
        <!-- End Box Head -->

         <!-- Table -->
            <div class="table">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class="odd">
                        <td><h3>First Name</h3></td>
                        <td><h3><?php echo $providerViewed->first_name;?></h3></td>
                        <td><h3>Last Name</h3></td>
                        <td><h3><?php echo $providerViewed->last_name;?></h3></td>
                    </tr>
                    <tr>
                        <td><h3>Phone</h3></td>
                        <td><h3><?php echo $providerViewed->phone;?></h3></td>
                        <td><h3>Email</h3></td>
                        <td><h3><?php echo $providerViewed->email;?></h3></td>
                    </tr>
                    <tr>
                        <td><h3>Address</h3></td>
                        <td colspan="3"><h3><?php echo $providerViewed->address;?></h3></td>
                        
                    </tr>
                    <tr class="odd">
                        <td><h3>City</h3></td>
                        <td><h3><?php echo $providerViewed->city;?></h3></td>
                        <td><h3>Postcode</h3></td>
                        <td><h3><?php echo $providerViewed->postcode;?></h3></td>
                    </tr>
                    
                    <tr class="odd">
                        <td><h3>Access Information</h3></td>
                        <td colspan="3"><h3><?php echo $providerViewed->access_information;?></h3></td>
                        
                    </tr>
                    <tr>
                        <td><h3>Longitude</h3></td>
                        <td><h3><?php echo $providerViewed->longitude;?></h3></td>
                        <td><h3>Latitude</h3></td>
                        <td><h3><?php echo $providerViewed->latitude;?></h3></td>
                    </tr>
                    <tr>
                        <td><h3>Monday</h3></td>
                        <td><h3><?php echo $providerViewed->monday_schedule;?></h3></td>
                        <td><h3>Tuesday</h3></td>
                        <td><h3><?php echo $providerViewed->tuesday_schedule;?></h3></td>
                    </tr>
                    <tr class="odd">
                        <td><h3>Wednesday</h3></td>
                        <td><h3><?php echo $providerViewed->wednesday_schedule;?></h3></td>
                        <td><h3>Thursday</h3></td>
                        <td><h3><?php echo $providerViewed->thursday_schedule;?></h3></td>
                    </tr>
                    <tr>
                        <td><h3>Friday</h3></td>
                        <td><h3><?php echo $providerViewed->friday_schedule;?></h3></td>
                        <td><h3>Saturday</h3></td>
                        <td><h3><?php echo $providerViewed->saturday_schedule;?></h3></td>
                    </tr>
                    <tr class="odd">
                        <td><h3>Sunday</h3></td>
                        <td><h3><?php echo $providerViewed->sunday_schedule;?></h3></td>
                        <td><h3>Status</h3></td>
                        <td><h3><?php echo $providerViewed->status;?></h3></td>
                    </tr>
                    <tr class="">
                        <td><h3>Public</h3></td>
                        <td><h3><?php echo $providerViewed->public;?></h3></td>
                        <td><h3>Longterm/Full day</h3></td>
                        <td><h3><?php echo $providerViewed->full_day;?></h3></td>
                    </tr>
                    <tr class="odd">
                        <td><h3>First Hour Rate</h3></td>
                        <td><h3>$<?php echo $providerViewed->rate_first_hour;?></h3></td>
                        <td><h3>First Day Rate</h3></td>
                        <td><h3>$<?php echo $providerViewed->rate_first_day;?></h3></td>
                       
                    </tr>
                    <tr class="">
                        <td><h3>Hourly Rate</h3></td>
                        <td><h3>$<?php echo $providerViewed->rate_per_hour;?></h3></td>
                        <td><h3>Daily Rate</h3></td>
                        <td><h3>$<?php echo $providerViewed->rate_per_day;?></h3></td>
                    </tr>
                    
                </table>
                <br class="clear">
                <h2 style="padding-left: 10px">Images:</h2>
                <p class="images">
                <?php global $SPOT_IMAGES_BASE_URL;
                    foreach ($images as $img){?>
                    <span class="left "><a  href="<?php echo $SPOT_IMAGES_BASE_URL.$img['path']; ?>">
                            <img src="<?php echo $SPOT_IMAGES_BASE_URL.$img['path']; ?>" /></a></span>
                <?php }?>
               </p>
               <br class="clear">
            </div>
            <!-- Table -->
        

    </div>
    <!-- End Box -->
</div>
<!-- End Content -->



<div class="cl">&nbsp;</div>
</div>
<!-- Main -->
</div>
</div>
<!-- End Container -->

<script type="text/javascript">
    $(document).ready(function(){
        $('a.del').on('click', function(e){
            e.preventDefault();
            if(confirm($(this).attr('data-confirm'))) {
                window.location = $(this).attr('href');
            }

            //return false;
        });

        $('.tooltip').tooltip();
     });
</script>
<?php include_layout_template('admin_footer.php'); ?>

