<?php
require_once('../includes/initialize.php');
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}

//$id_user = isset($_GET['userid']) ? $_GET['userid'] : 0;
//$id_vhcl = isset($_GET['id']) ? $_GET['id'] : 0;
$msg = isset($_GET['msg']) ? $_GET['msg'] : '';

$userO = User::getInstance();
$user = $userO->find_by_id($session->user_id);
//$userViewed = $userO->find_by_id($id_user);

?>

<?php render_layout_template('admin_header.php', array('session' => $session, 'user' => $user, 'is_booking_page'=>true)); ?>

<!-- Container -->
<div id="container">
<div class="shell">

<!-- Small Nav -->
<div class="small-nav">
    <a href="user.php">Dashboard</a>
    <span>&gt;</span>
    Bookings
</div>
<!-- End Small Nav -->


<br/>
<!-- Main -->
<div id="main">
<div class="cl">&nbsp;</div>

<!-- Content -->
<div id="content">

    <!-- Box -->
    <div class="box">
        <!-- Box Head -->
        <div class="box-head">
            <h2 class="left">Bookings</h2>
        </div>
        
        <!-- End Box Head -->

        <?php if ( strlen($session->message())): ?>
            <!-- Message OK -->
            <div class="msg msg-ok">
                <p><strong><?php echo $session->message(); ?></strong></p>
            </div>
            <!-- End Message OK -->
        <?php endif; ?>
        <?php
        
            $bookings = User::getAllBookings();
        ?>
        <?php if (!empty($bookings)) : ?>
            <!-- Table -->
            <div class="table">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <th>Booked At  </th>
                        <th>Date in  </th>
                        <th>Date out</th>
                        <th>Fee</th>
                        <th>User</th>
                        <th>Spot</th>                       
                        
                    </tr>

                    <?php foreach ($bookings as $k => $b) : ?>

                        <tr <?php echo ($k%2 != 0) ? 'class="odd"' : ''; ?> >
                            <td><h3><?php echo $b['timestamp'] ?></h3></td>
                            <td><h3><?php echo $b['start_datetime'] ?></h3></td>
                            <td><h3><?php echo $b['end_datetime'] ?></h3></td>
                            <td><h3><?php echo $b['price'] ?></h3></td>
                            <td><h3><a  href="view_user.php?id=<?php echo $b['user_id'] ?>"><?php echo $b['u_first_name'] . ' '. $b['u_last_name'] ?></a></h3></td>
                            <td><h3><a  href="view_provider.php?id=<?php echo $b['provider_id'] ?>"><?php echo $b['s_first_name'] . ' '. $b['s_last_name'] ?></a></h3></td>

                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <!-- Table -->
        <?php else : ?>
            <p>There are no Bookings at the moment!</p>
        <?php endif; ?>

    </div>
    <!-- End Box -->

    <!-- Box -->
    
    <!-- End Box -->

</div>
<!-- End Content -->



<div class="cl">&nbsp;</div>
</div>
<!-- Main -->
</div>
</div>
<!-- End Container -->

<script type="text/javascript">
    $(document).ready(function(){
        $('a.del').on('click', function(e){
            e.preventDefault();
            if(confirm($(this).attr('data-confirm'))) {
                window.location = $(this).attr('href');
            }

            //return false;
        });

        $('.tooltip').tooltip();
     });
</script>
<?php include_layout_template('admin_footer.php'); ?>

