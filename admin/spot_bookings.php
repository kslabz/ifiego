<?php
require_once('../includes/initialize.php');
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}

$id_provider = isset($_GET['providerid']) ? $_GET['providerid'] : 0;

$msg = isset($_GET['msg']) ? $_GET['msg'] : '';

$userO = User::getInstance();
$user = $userO->find_by_id($session->user_id);
$providerViewed = Provider::find_by_id($id_provider);
//$additional_info = '<a href="#" class="tooltip" title="'.$schedule_format.'"><img src="'.rtrim($base_url,'/').'/media/admin/images/info-icon.png" alt="" /></a>';

?>

<?php render_layout_template('admin_header.php', array('session' => $session, 'user' => $user, 'is_booking_page'=>true)); ?>

<!-- Container -->
<div id="container">
<div class="shell">

<!-- Small Nav -->
<div class="small-nav">
    <a href="dashboard.php">Provider</a>
    <span>&gt;</span>
    <?php echo $providerViewed->first_name.' '.$providerViewed->last_name; ?>
    <span>&gt;</span>
    Bookings
</div>
<!-- End Small Nav -->


<br/>
<!-- Main -->
<div id="main">
<div class="cl">&nbsp;</div>

<!-- Content -->
<div id="content">

    <!-- Box -->
    <div class="box">
        <!-- Box Head -->
        <div class="box-head">
            <h2 class="left"><?php echo $providerViewed->first_name . ' ' .$providerViewed->last_name;?>'s Bookings</h2>
        </div>
        <!-- End Box Head -->

        <?php if ( strlen($session->message())): ?>
            <!-- Message OK -->
            <div class="msg msg-ok">
                <p><strong><?php echo $session->message(); ?></strong></p>
            </div>
            <!-- End Message OK -->
        <?php endif; ?>
        <?php

            $bookings = Provider::getProviderBookings($id_provider);
        ?>
        <?php if (!empty($bookings)) : ?>
            <!-- Table -->
            <div class="table">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <th>Booked At  </th>
                        <th>Start datetime  </th>
                        <th>End datetime</th>
                        <th>User</th>                       
                        
                    </tr>

                    <?php foreach ($bookings as $k => $b) : ?>

                        <tr <?php echo ($k%2 != 0) ? 'class="odd"' : ''; ?> >
                            <td><h3><?php echo $b['timestamp'] ?></h3></td>
                            <td><h3><?php echo $b['start_datetime'] ?></h3></td>
                            <td><h3><?php echo $b['end_datetime'] ?></h3></td>
                            <td><h3><a  href="user.php?id=<?php echo $b['user_id'] ?>#form-user">View</a></h3></td>

                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <!-- Table -->
        <?php else : ?>
            <p>There are no Bookings at the moment!</p>
        <?php endif; ?>

    </div>
    <!-- End Box -->

    <!-- Box -->
    
    <!-- End Box -->

</div>
<!-- End Content -->



<div class="cl">&nbsp;</div>
</div>
<!-- Main -->
</div>
</div>
<!-- End Container -->

<script type="text/javascript">
    $(document).ready(function(){
        $('a.del').on('click', function(e){
            e.preventDefault();
            if(confirm($(this).attr('data-confirm'))) {
                window.location = $(this).attr('href');
            }

            //return false;
        });

        $('.tooltip').tooltip();
     });
</script>
<?php include_layout_template('admin_footer.php'); ?>

