<?php

require_once("../includes/initialize.php");



if ($session->is_logged_in()) {

    redirect_to("dashboard.php");

}



// Remember to give your form's submit tag a name="submit" attribute!

if (! strcmp($_SERVER['REQUEST_METHOD'],'POST')) { // Form has been submitted.



    $username = trim($_POST['username']);

    $password = trim($_POST['password']);



    // Check database to see if username/password exist.

    $found_user = User::authenticate($username, $password, true);



    if ($found_user) {

        $session->login($found_user);

        log_action('Login', "{$found_user->username} logged in.");

        redirect_to("dashboard.php");

    } else {

        // username/password combo was not found in the database

        $message = "Username/password combination incorrect.";

    }

} else { // Form has not been submitted.

    $username = "";

    $password = "";

}



?>

<?php render_layout_template('admin_header.php', array('session' => $session)); ?>



<!-- Container -->

<div id="container">

    <div class="shell">





        <br/>

        <!-- Main -->

        <div id="main">

            <div class="cl">&nbsp;</div>



            <!-- Content -->

            <div id="content">



                <!-- Box -->

                <div class="box">

                    <!-- Box Head -->

                    <div class="box-head">

                        <h2>Login</h2>

                    </div>

                    <!-- End Box Head -->





                    <?php echo output_message($message); ?>



                    <form action="login.php" method="post">



                        <!-- Form -->

                        <div class="form">

                            <p>

                                <label>Username</label>

                                <input type="text" name="username" maxlength="30"

                                       value="<?php echo htmlentities($username); ?>" class="field size5"/>

                            </p>



                            <p>

                                <label>Password</label>

                                <input type="password" name="password" maxlength="30"

                                       value="<?php echo htmlentities($password); ?>" class="field size5"/>

                            </p>

                        </div>

                        <!-- End Form -->



                        <!-- Form Buttons -->

                        <div class="buttons">

                            <input type="submit" name="submit" class="button left" value="Login"/>



                            <div style="clear:both;"></div>

                        </div>



                        <!-- End Form Buttons -->

                    </form>



                </div>

                <!-- End Box -->



            </div>

            <!-- End Content -->



            <div class="cl">&nbsp;</div>

        </div>

        <!-- End Main -->

    </div>



</div>

<!-- End Container -->



<?php include_layout_template('admin_footer.php'); ?>

