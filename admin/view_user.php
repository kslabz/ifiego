<?php
require_once('../includes/initialize.php');
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}

//$id_user = isset($_GET['userid']) ? $_GET['userid'] : 0;
$user_id = isset($_GET['id']) ? $_GET['id'] : 0;

$userO = User::getInstance();
$user = $userO->find_by_id($session->user_id);
$userViewed = User::find_by_id($user_id);
$dashboard = '';
if($userViewed->type == 'admin')
{
    $dashboard = 'Admin';
    $user_page = 'is_adminuser_page';
}    
else{ 
    $user_page = 'is_user_page';
}
?>

<?php render_layout_template('admin_header.php', array('session' => $session, 'user' => $user,$user_page =>true)); ?>

<!-- Container -->
<div id="container">
<div class="shell">

<!-- Small Nav -->
<div class="small-nav">
    <a href="user.php"><?php echo $dashboard;?> Users</a>
    <span>&gt;</span>
    <?php echo $userViewed->first_name . ' ' .$userViewed->last_name;?>
</div>
<!-- End Small Nav -->


<br/>
<!-- Main -->
<div id="main">
<div class="cl">&nbsp;</div>

<!-- Content -->
<div id="content">

    <!-- Box -->
    <div class="box">
        <!-- Box Head -->
        <div class="box-head">
            <h2 class="left"><?php echo $userViewed->first_name . ' ' .$userViewed->last_name;?> </h2> <span class="right"><a class="ico edit" href="user.php?id=<?php echo $userViewed->id; ?>#form-user">Edit</a></span>
        </div>
        <!-- End Box Head -->

         <!-- Table -->
            <div class="table">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class="odd">
                        <td><h3>First Name</h3></td>
                        <td><h3><?php echo $userViewed->first_name;?></h3></td>
                        <td><h3>Last Name</h3></td>
                        <td><h3><?php echo $userViewed->last_name;?></h3></td>
                    </tr>
                    <tr>
                        <td><h3>Phone</h3></td>
                        <td><h3><?php echo $userViewed->phone;?></h3></td>
                        <td><h3>Email</h3></td>
                        <td><h3><?php echo $userViewed->email;?></h3></td>
                    </tr>
                    
                    
                    <tr class="odd">
                        <td><h3>Status</h3></td>
                        <td><h3><?php echo $userViewed->status;?></h3></td>
                    </tr>
                    
                </table>
                
            </div>
            <!-- Table -->
        

    </div>
    <!-- End Box -->
</div>
<!-- End Content -->



<div class="cl">&nbsp;</div>
</div>
<!-- Main -->
</div>
</div>
<!-- End Container -->

<script type="text/javascript">
    $(document).ready(function(){
        $('a.del').on('click', function(e){
            e.preventDefault();
            if(confirm($(this).attr('data-confirm'))) {
                window.location = $(this).attr('href');
            }

            //return false;
        });

        $('.tooltip').tooltip();
     });
</script>
<?php include_layout_template('admin_footer.php'); ?>

