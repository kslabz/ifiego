<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>

    <title>Searchable Address Admin</title>

    <link rel="stylesheet" href="../media/admin/style.css" type="text/css" media="all"/>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>

    <script type="text/javascript" src="../media/admin/js/jquery-ui.js"></script>



</head>

<body>

<!-- Header -->

<div id="header">

    <div class="shell">

        <!-- Logo + Top Nav -->

        <div id="top">

            <h1><a href="#">Searchable Address</a></h1>

            <?php if ($session->is_logged_in()) : ?>

                <div id="top-navigation">

                    Welcome <strong><?php echo $user->full_name(); ?></strong>

                    <span>|</span>

                    <a href="logout.php">Log out</a>

                </div>

            <?php endif; ?>

        </div>

        <!-- End Logo + Top Nav -->



        <?php if ($session->is_logged_in()) : ?>

            <!-- Main Nav -->

            <div id="navigation">

                <ul>
                    <li><a href="dashboard.php" class="<?php if($is_dashboard_page){echo 'active';}?>"><span>Parking spots</span></a></li>
                    <li><a href="bookings.php" class="<?php if($is_booking_page){echo 'active';}?>"><span>Bookings</span></a></li>
                    <li><a href="user.php" class="<?php if($is_user_page){echo 'active';}?>"><span>Users</span></a></li>
                    <li><a href="admin_user.php" class="<?php if($is_adminuser_page){echo 'active';}?>"><span>Admin Users</span></a></li>
                </ul>

            </div>

            <!-- End Main Nav -->

        <?php endif; ?>

    </div>

</div>

<!-- End Header -->