
<!-- Footer -->
<div id="footer">
    <div class="shell">
        <span class="left">&copy; <?php echo date("Y", time()); ?> - Searchable Address</span>
		<span class="right">
			Design by <a href="http://chocotemplates.com" target="_blank" title="The Sweetest CSS Templates WorldWide">Chocotemplates.com</a>
		</span>
    </div>
</div>
<!-- End Footer -->

</body>
</html>


<?php if (isset($database)) {
    $database->close_connection();
} ?>