<div id="header" class="container">
    <div id="logo">
        <h1><a href="index.php">Searchable Address</a></h1>
    </div>
    <div id="menu">
        <?php $currentPage = str_replace('/'.MAIN_FOLDER.'/', '', $_SERVER['PHP_SELF']);//Current Page URI ?>
        <ul>
            <li <?php echo ($currentPage == '' || $currentPage == 'index.php') ? 'class="active"' : '' ?>><a href="index.php" accesskey="1">Home</a></li>
            <li <?php echo ($currentPage == 'provider.php') ? 'class="active"' : '' ?>><a href="provider.php" accesskey="2">Provider</a></li>
        </ul>
    </div>
</div>