-- phpMyAdmin SQL Dump
-- version 4.0.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 08, 2013 at 08:01 PM
-- Server version: 5.0.96-community-log
-- PHP Version: 5.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `x23lrndz_searchable`
--

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE IF NOT EXISTS `providers` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `first_name` varchar(100) default NULL,
  `last_name` varchar(100) default NULL,
  `phone` varchar(16) default NULL,
  `email` varchar(100) default NULL,
  `address` varchar(100) default NULL,
  `city` varchar(100) default NULL,
  `postcode` varchar(10) default NULL,
  `access_information` text,
  `monday_schedule` varchar(100) default NULL,
  `tuesday_schedule` varchar(100) default NULL,
  `wednesday_schedule` varchar(100) default NULL,
  `thursday_schedule` varchar(100) default NULL,
  `friday_schedule` varchar(100) default NULL,
  `saturday_schedule` varchar(100) default NULL,
  `sunday_schedule` varchar(100) default NULL,
  `longitude` decimal(11,8) default NULL,
  `latitude` decimal(11,8) default NULL,
  `rate_per_minute` decimal(10,2) default NULL,
  `status` enum('active','inactive') default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`id`, `first_name`, `last_name`, `phone`, `email`, `address`, `city`, `postcode`, `access_information`, `monday_schedule`, `tuesday_schedule`, `wednesday_schedule`, `thursday_schedule`, `friday_schedule`, `saturday_schedule`, `sunday_schedule`, `longitude`, `latitude`, `rate_per_minute`, `status`) VALUES
(1, 'Irfan', 'GHAURI', '0606060606', 'xx@yy.com', '7 Avenue de Flirey', 'Nice', '06000', 'Contact the renter', '09:00 -18:00', '09:00 -18:00', '09:00 -18:00', '09:00 -18:00', '09:00 -18:00', '09:00 -14:00', '09:00 -14:00', 7.27515260, 43.72183070, 0.00, 'inactive'),
(2, 'Frederique', 'Olivier', '0619282122', 'fg@yahoo.com', '31 Avenue Cap de Croix', 'Nice', '06000', 'Acces par Bip Appeler le proprietaire', '9:00-12:00', '14:00-18:00', '14:00-18:00', '9:00-12:00', '9:00-12:00', '00:00-00:00', '00:00-00:00', 7.27619980, 43.72797280, 0.00, 'inactive'),
(3, 'John', 'Khan', '0618920668', 'irgh2238@free.fr', '18 Boulevard Dubouchage', 'Nice', '06000', 'Appeler le proprietaire', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', 7.27123610, 43.70167060, 0.00, 'active'),
(4, 'Charles', 'Dupot', '0619282222', 'ir_gh@yahoo.com', '12 rue biscarra', 'Nice', '06000', 'Contacter le proprietaire', '9:00-12:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', 7.26942680, 43.70295780, 0.00, 'active'),
(6, 'Pierre-Yves', 'Triquier', '0619282123', 'pyves@nmail.com', '41 promenade des anglais', 'nice', '06000', 'Contacter le proprietaire', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', 7.25661280, 43.69393550, 0.07, 'active'),
(7, 'Genevieve', 'Asloum', '0619282124', 'genevieve@test.com', '21 boulevard gorbella', 'nice', '06000', 'contacter le propietaire', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', 7.25659830, 43.71959690, 0.05, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(50) NOT NULL,
  `password` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`) VALUES
(1, 'irfan', '$2pw5Oo.zGbZs', 'Irfan', 'Ghauri');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
         