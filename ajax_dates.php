﻿<?php require_once("includes/initialize.php"); ?>

<?php
$date = $_POST['date'];
$day = date('D',strtotime($date));
$provider_id = $_POST['provider_id'];

$provider = Provider::find_by_id($provider_id);
$data = array();
if (!empty($provider)) {
    switch ($day) {
        case 'Mon': $schedule = $provider->monday_schedule; break;
        case 'Tue': $schedule = $provider->tuesday_schedule; break;
        case 'Wed': $schedule = $provider->wednesday_schedule; break;
        case 'Thu': $schedule = $provider->thursday_schedule; break;
        case 'Fri': $schedule = $provider->friday_schedule; break;
        case 'Sat': $schedule = $provider->saturday_schedule; break;
        case 'Sun': $schedule = $provider->sunday_schedule; break;
        default: $schedule = '';
    }
}

if (strlen($schedule) && $schedule!= '00:00-00:00'){
    $hours = explode('-', $schedule);
    $start = explode(':', $hours[0]);
    $start_hour = (int) $start[0];
    $end = explode(':', $hours[1]);
    $end_hour = (int) $end[0];

    $data['start'] = '';
    while(strtotime($hours[0]) <= strtotime($hours[1])){
        $prev = date('H:i', strtotime($hours[0])); // format the start time

        $next = strtotime('+15mins', strtotime($hours[0])); // add 15 mins
        $hours[0] = date('H:i', $next);

        $data['start'] .='<option value="'.($prev).'" >'. $prev .'</option>';

    }
    $data['end'] = $data['start'];
}

echo json_encode($data);


?>