﻿<?php

//get data
require_once('includes/database_webservice.php');
require_once('includes/functions.php');
mysql_select_db(DB_NAME, $db_webservice);

######## Fetching data in UTF 8 format from database ###########
mysql_query("SET NAMES 'utf8'");
mysql_query('SET CHARACTER SET utf8');

//latitude
if (isset($_GET['latitude'])) {
  $latitude = $_GET['latitude'];
}
else if (isset($_POST['latitude'])) {
  $latitude = $_POST['latitude'];
}

//longitude
if (isset($_GET['longitude'])) {
  $longitude = $_GET['longitude'];
}
else if (isset($_POST['longitude'])) {
  $longitude = $_POST['longitude'];
}

//X - km
if (isset($_GET['X'])) {
  $x = $_GET['X'];
}
else if (isset($_POST['X'])) {
  $x = $_POST['X'];
}

if (isset($latitude) && isset($longitude) && isset($x)) {
    $x = (float) $x;
    if ($x > 0) {
        //select all providers
        $query ="SELECT * FROM `providers`";
        $rsProviders = mysql_query($query, $db_webservice) or
          die(mysql_error());

        $latArray = array();
        $longArray = array();
        $keys = array();
        while ($row_rsProviders = mysql_fetch_assoc($rsProviders)) {//keep longitude and latitude of all providers in arrays
            if ($row_rsProviders['latitude'] != 0 && $row_rsProviders['longitude'] !=0 && $row_rsProviders['status'] == 'active'){
                $latArray[] = $row_rsProviders['latitude'];
                $longArray[] = $row_rsProviders['longitude'];
                $keys[] = $row_rsProviders['id'];
            }
        }

        $finalKeys = array();
        $distance = array();
        foreach ($latArray as $key => $val) {//calculate distance => store only the ones with distance < $x
            $distance = haversineGreatCircleDistance($latArray[$key], $longArray[$key], $latitude, $longitude) / 1000; // km
            if ($distance <= $x) {
                $finalKeys[] = $keys[$key];
            }
        }

        $arRows = array();
        if (!empty($finalKeys)) { // final query
            $finalQuery ="SELECT * FROM `providers` WHERE `id` IN ('" . implode("','", $finalKeys) . "')";
            $finalProviders = mysql_query($finalQuery, $db_webservice) or die(mysql_error());
            while ($row_rsProviders = mysql_fetch_assoc($finalProviders)) {
                unset($row_rsProviders['id']);
                array_push($arRows, $row_rsProviders);
            }
        }

        header('Content-type: application/json');
        echo json_encode($arRows);
    }
    else {
        echo 'X must be a positive number.';
    }
}
else {
    echo 'Missing parameters.';
}



?>
