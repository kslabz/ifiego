﻿<?php require_once("includes/initialize.php"); ?>

<?php

$lat = 0;
$long = 0;

$errorMessage = '';
if (! strcmp($_SERVER['REQUEST_METHOD'],'POST')) {

    $keyword = trim(strip_tags($_POST['search']));

    $url = 'http://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode($keyword.', France');
    $data = json_decode(file_get_contents($url), true);


    if($data['status']='OK'){
        $latLong =  $data['results'][0]['geometry']['location'];
        $lat = $latLong['lat'];
        $long = $latLong['lng'];
    } else {
        $errorMessage = 'The location can\'t be found' ;

    }

}

$select = "SELECT * FROM `providers`";
$where = array();
$where[] = "`status` = '".Provider::STATUS_ACTIVE."'";


$query = $select ." ". (!empty($where) ? " WHERE " .implode(" AND ", $where) : " ");


$providers = Provider::find_by_sql($query);

$finalProviders = array();
if (!empty($providers)) {

    if ($lat != 0 && $long != 0) {
        $latArray = array();
        $longArray = array();
        $keys = array();
        foreach ($providers as $key => $provider) {
            if ($provider->latitude != 0 && $provider->longitude !=0){
                 $latArray[] = $provider->latitude;
                 $longArray[] = $provider->longitude;
                 $keys[] = $key;
            }

        }
        $finalKeys = array();
        $distance = array();
        foreach ($latArray as $key => $val) {
            $distance = haversineGreatCircleDistance($latArray[$key], $longArray[$key], $lat, $long) / 1000; // km
            if ($distance <= 50) {
                $finalKeys[] = $keys[$key];
            }
        }
        if (!empty($finalKeys)) {
            foreach ($finalKeys as $key) {
                $finalProviders[] = $providers[$key];
            }
        }
        else{
            $errorMessage = 'There are no providers in the neighbourhood. (50km)';
        }

    } else { // display all
        if (!strlen($errorMessage)) {
            $finalProviders = $providers;
        }

    }

}
else {
    $errorMessage = 'There are no providers in the database';
}

?>

<?php include_layout_template('head_includes.php'); ?>
<?php include_layout_template('header.php'); ?>

   <div id="page" class="container">
    <div id="content">
        <h2>Welcome to searchable address!</h2>
        <?php if (isset($keyword)) : ?>
            <h2>Searched for: "<?php echo $keyword; ?>"</h2>
        <?php endif; ?>
        <?php if (!empty($finalProviders)) : ?>


            <div class="google-map" style="border:none;" >

                <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
                <script type="text/javascript">
                    $(document).ready(function() {
                        initialize();
                    });


                    function initialize() {

                        var map_options = {
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };

                        var google_map = new google.maps.Map(document.getElementById("map_canvas"), map_options);

                        var info_window = new google.maps.InfoWindow({
                            content: 'loading'
                        });

                        var x = [];
                        var y = [];
                        var h = [];

                        var locations= <?php echo json_encode($finalProviders); ?>;

                        for(var i=0;i<locations.length;i++){
                            x.push(locations[i].latitude);
                            y.push(locations[i].longitude);
                            h.push('<h2>'+locations[i].first_name+ ' '+locations[i].last_name +'</h2><br><p>'+locations[i].address+ ' '+locations[i].city + ' '+locations[i].postcode+'</p>');
                        }


                        var bounds = new google.maps.LatLngBounds();

                        var j = 0;
                        for ( item in x ) {
                            var m = new google.maps.Marker({
                                map:       google_map,
                                animation: google.maps.Animation.DROP,
                                position:  new google.maps.LatLng(parseFloat(x[j]),parseFloat(y[j])),
                                html:      h[j]
                            });

                            // set bounds
                            bounds.extend(m.position);

                            google.maps.event.addListener(m, 'click', function() {
                                info_window.setContent(this.html);
                                info_window.open(google_map, this);
                            });


                            var listener = google.maps.event.addListener(google_map, "bounds_changed", function() {
                                google.maps.event.removeListener(listener);
                                google_map.setZoom( Math.min( 15, google_map.getZoom() ) );
                            });
                            google_map.fitBounds(bounds);

                            j++;
                        }

                    }
                </script>


                <div id="map_canvas" style="width:663px;height:408px; border:1px solid #d1d2d2; border-radius:2px;margin-top: 30px;" >Google Map</div>


            </div>

            <br><br>
            <div id="providers-results">
                <?php foreach ($finalProviders as $key => $provider) : ?>
                    <div class="results-box left" style="margin-right: 40px;">
                        <h2><?php echo $provider->first_name.' '.$provider->last_name; ?></h2><br>
                        <?php echo $provider->address; ?><br>
                        <?php echo $provider->city.', '. $provider->postcode; ?><br>
                        <?php echo 'Rate per minute: $' . $provider->rate_per_minute;?><br>
                        <a href="reserve.php?id=<?php echo $provider->id; ?>">Book Now</a>
                    </div>
                <?php endforeach; ?>

            </div>
        <?php else: ?>
            <p><?php echo $errorMessage; ?></p>
        <?php endif; ?>

    </div>
    <div id="sidebar">
        <h2>Where are you looking?</h2>
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
            <input type="text" name="search" value="">
            <p>within 50km</p>
            <input type="submit" name="search_submit" value="Search">

        </form>
    </div>
</div>

<?php include_layout_template('footer.php'); ?>