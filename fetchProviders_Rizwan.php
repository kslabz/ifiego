﻿<?php

	header('Content-Type: text/html; charset=utf-8');
	
	require_once 'includes/initialize.php';
	
	$mainArr = array();
	
	######## Fetching data in UTF 8 format from database ###########
	mysql_query("SET NAMES 'utf8'"); 
	mysql_query('SET CHARACTER SET utf8');
	
	/* Fetching provider data */	
	$dbQry = "SELECT * FROM `providers`";
	$fetchObj = mysql_query($dbQry);
	$regArray = array();
	
	while($row = mysql_fetch_assoc($fetchObj))
	{
		$temp = array();
		
		$temp['first_name'] 			= 	$row['first_name'];
		$temp['last_name'] 				= 	$row['last_name'];
		$temp['phone'] 					= 	$row['phone'];
		$temp['email'] 					= 	$row['email'];
		$temp['address'] 				= 	$row['address'];
		$temp['city'] 					= 	$row['city'];
		$temp['postcode'] 				= 	$row['postcode'];
		$temp['access_information'] 	= 	$row['access_information'];
		$temp['monday_schedule'] 		= 	$row['monday_schedule'];
		$temp['tuesday_schedule'] 		= 	$row['tuesday_schedule'];
		$temp['wednesday_schedule'] 	= 	$row['wednesday_schedule'];
		$temp['thursday_schedule'] 		= 	$row['thursday_schedule'];
		$temp['friday_schedule'] 		= 	$row['friday_schedule'];
		$temp['saturday_schedule'] 		= 	$row['saturday_schedule'];
		$temp['sunday_schedule'] 		= 	$row['sunday_schedule'];
		$temp['longitude'] 				= 	$row['longitude'];
		$temp['latitude'] 				= 	$row['latitude'];
		$temp['rate_per_minute'] 		= 	$row['rate_per_minute'];
		$temp['status'] 				= 	$row['status'];
		
		$regArray[] = $temp;
	}
	
	$mainArr["result"] = $regArray;	
	print(json_encode($mainArr));
?> 
