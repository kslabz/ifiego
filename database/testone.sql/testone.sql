-- phpMyAdmin SQL Dump
-- version 3.3.10
-- http://www.phpmyadmin.net
--
-- Host: testone.sql-pro.online.net
-- Generation Time: Apr 17, 2014 at 07:03 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.2-1ubuntu4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `testone`
--

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE IF NOT EXISTS `providers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postcode` varchar(10) DEFAULT NULL,
  `access_information` text,
  `monday_schedule` varchar(100) DEFAULT NULL,
  `tuesday_schedule` varchar(100) DEFAULT NULL,
  `wednesday_schedule` varchar(100) DEFAULT NULL,
  `thursday_schedule` varchar(100) DEFAULT NULL,
  `friday_schedule` varchar(100) DEFAULT NULL,
  `saturday_schedule` varchar(100) DEFAULT NULL,
  `sunday_schedule` varchar(100) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `latitude` decimal(11,8) DEFAULT NULL,
  `rate_per_minute` decimal(10,2) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`id`, `first_name`, `last_name`, `phone`, `email`, `address`, `city`, `postcode`, `access_information`, `monday_schedule`, `tuesday_schedule`, `wednesday_schedule`, `thursday_schedule`, `friday_schedule`, `saturday_schedule`, `sunday_schedule`, `longitude`, `latitude`, `rate_per_minute`, `status`) VALUES
(1, 'Irfan', 'GHAURI', '0606060606', 'xx@yy.com', '7 Avenue de Flirey', 'Nice', '06000', 'Contact the renter', '09:00-18:00', '09:00-18:00', '09:00-18:00', '09:00-18:00', '09:00-18:00', '09:00-14:00', '09:00-14:00', 7.27515260, 43.72183070, 0.00, 'active'),
(30, 'Frederique', 'Olivier', '0618920669', 'ir_gh@yahoo.com', '31 Avenue Cap de Croix', 'Nice', '06100', 'contact owner', '09:00-16:00', '09:00-16:00', '00:00-00:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', 7.27619980, 43.72797280, 0.08, 'active'),
(3, 'John', 'Khan', '0618920668', 'irgh2238@free.fr', '18 Boulevard Dubouchage', 'Nice', '06000', 'Appeler le proprietaire', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', 7.27123610, 43.70167060, 0.00, 'active'),
(4, 'Charles', 'Dupot', '0619282222', 'ir_gh@yahoo.com', '12 rue biscarra', 'Nice', '06000', 'Contacter le proprietaire', '9:00-12:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', 7.26942680, 43.70295780, 0.00, 'active'),
(6, 'Pierre-Yves', 'Triquier', '0619282123', 'pyves@nmail.com', '41 promenade des anglais', 'nice', '06000', 'Contacter le proprietaire', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', 7.25661280, 43.69393550, 0.07, 'active'),
(7, 'Genevieve', 'Asloum', '0619282124', 'genevieve@test.com', '21 boulevard gorbella', 'nice', '06000', 'contacter le propietaire', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', 7.25659830, 43.71959690, 0.05, 'active'),
(8, 'Paper', 'Tiger', '0619282122', 'irgh2238@free.fr', '407, avenue sainte marguerite', 'Nice', '06200', 'Call', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', 7.19799450, 43.70472560, 10.00, 'active'),
(9, 'Michel', 'Tabira', '0619282128', 'masoupe@free.fr', '112 chemin des collettes', 'Cagnes sur Mer', '06800', 'contact owner', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', 7.15363930, 43.67989260, 0.08, 'active'),
(5, 'Peter', 'Malm', '0618920322', 'irgh2238@free.fr', '26 rue giofreddo', 'Nice', '06000', 'Contact provider', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', 7.27547520, 43.70070390, 0.08, 'active'),
(10, 'Rizwan', 'Khan', '0606060606', 'rizwan@gmail.com', 'Dummy Address by Rizwan for testing', 'Cagnes sur Mer', '06800', 'Contact the renter', '09:00-18:00', '09:00-18:00', '09:00-18:00', '09:00-18:00', '09:00-18:00', '00:00-00:00', '00:00-00:00', 7.15702000, 43.66724000, 0.08, 'active'),
(11, 'Rizwan', 'Siddique', '0619282122', 'rizwansiddique@yahoo.com', 'Dummy Address by Rizwan for testing', 'Nice', '06000', 'Acces par Bip Appeler le proprietaire', '9:00-12:00', '14:00-18:00', '14:00-18:00', '9:00-12:00', '9:00-12:00', '00:00-00:00', '00:00-00:00', 7.37123610, 43.69167060, 0.00, 'inactive'),
(12, 'Imran', 'Khan', '0618920668', 'imrankhan@free.fr', 'Dummy Address by Rizwan for testing', 'Nice', '06000', 'Appeler le proprietaire', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', 7.37123610, 43.71167060, 0.00, 'inactive'),
(13, 'Philippe', 'Gresham', '0619282199', 'masoupe@free.fr', '11 avenue de la californie', 'nice', '06200', 'contact provider', '09:00-17:00', '09:00-18:00', '00:00-00:00', '09:00-19:00', '00:00-00:00', '00:00-00:00', '00:00-00:00', 7.24422950, 43.69080520, 0.09, 'active'),
(31, 'Jacques', 'Cholet', '0603452344', 'j.cholet@yahoo.com', '13 rue de tolbiac', 'paris', '75013', 'contact provider', '09:00-16:00', '09:00-16:00', '09:00-16:00', '00:00-00:00', '00:00-00:00', '09:00-18:00', '00:00-00:00', 2.37458150, 48.82937370, 0.10, 'active'),
(28, 'Nabil', 'Kherrine', '0618924820', 'masoupe@free.fr', '32 avenue marecharl juin', 'Cagnes sur Mer', '06800', 'contact owner', '08:00-16:00', '09:00-18:00', '11:00-16:00', '00:00-00:00', '00:00-00:00', '00:00-00:00', '00:00-00:00', 7.15068310, 43.65867070, 0.09, 'active'),
(33, 'Georges', 'Clemenceau', '33618920668', 'ghauriirfan@gmail.com', '455 promenade des anglais', 'Nice', '06200', 'information', '09:00-18:00', '09:00-18:00', '09:00-18:00', '00:00-00:00', '00:00-00:00', '00:00-00:00', '00:00-00:00', 7.21377020, 43.66747890, 0.09, 'active'),
(34, 'Irfan', 'GHAURI', '+33618920668', 'ghauriirfan@gmail.com', '6 rue massnet', 'Nice', '06000', 'Appeler le', '09:00-19:00', '09:00-19:00', '09:00-19:00', '09:00-19:00', '09:00-19:00', '09:00-19:00', '09:00-19:00', 7.26417660, 43.69616720, 0.05, 'active'),
(35, 'Irfan', 'GHAURI', '0618920668', 'ghauriirfan@gmail.com', '21 rue meyerbeer', 'Nice', '06000', 'Contact owner', '09:00-19:00', '09:00-19:00', '09:00-19:00', '09:00-19:00', '09:00-19:00', '09:00-19:00', '09:00-19:00', 7.26046580, 43.69700970, 0.08, 'active'),
(41, 'eqe', 'qweqw', '5555', 'ir_gh@yahoo.com', '2 aveune kleber', 'Paris', '75008', 'eqwqw', '08:00-14:00', '00:00-00:00', '00:00-00:00', '00:00-00:00', '00:00-00:00', '00:00-00:00', '00:00-00:00', 2.29401840, 48.87277720, 0.08, 'inactive'),
(42, 'Emmanuel', 'Rosenfeld', '+33642332333', 'ir_gh@yahoo.com', '42 Rue Paul Deroulede', 'Nice', '06000', 'Call 0751310470', '09:00-18:00', '09:00-18:00', '09:00-18:00', '09:00-18:00', '09:00-18:00', '08:00-18:00', '08:00-18:00', 7.26240010, 43.70087630, 0.02, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`) VALUES
(1, 'irfan', '$2pw5Oo.zGbZs', 'Irfan', 'Ghauri');
