CREATE TABLE IF NOT EXISTS `provider_images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `provider_id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `provider_id` (`provider_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;


ALTER TABLE `provider_images`
  ADD CONSTRAINT `provider_images_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`);