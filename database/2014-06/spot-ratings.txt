ALTER TABLE  `spot_ratings` DROP FOREIGN KEY  `spot_ratings_ibfk_1` ,
ADD FOREIGN KEY (  `user_id` ) REFERENCES  `testone`.`users` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `spot_ratings` DROP FOREIGN KEY  `spot_ratings_ibfk_2` ,
ADD FOREIGN KEY (  `provider_id` ) REFERENCES  `testone`.`providers` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

