﻿ERROR CODES:
1 : not valid API call method
2 : username required
3 : password required
4 : email required
5 : username/password does not match
6 : not authorized
7 : user already exists
8 : required parameters missing


APIs:
auth/signin:
URL: /api/user.php
Required Params(POST key-value):
	method: "auth"
	username: <username which is email>
	password: <password>
Response(JSON):
{
    "status": 1,
    "errorno": 0,
    "contents": {
        "user_id": "5",
        "first_name": "Irfan",
        "last_name": "lastName"
    }
}


signup:
URL: /api/user.php
Required Params(POST key-value):
	method: "signup"
	email: <email>
	password: <passwod>
	phone: <optional phone no>
	first_name: <user's first name>
	last_name: <user's last name>
Response(JSON):
{
    "status": 1,
    "errorno": 0,
    "contents": {
        "user_id": <user's id in DB>
    }
}


list providers:
URL: /api/provider.php
Required Params(POST key-value):
	method: "list"
	username: <email>
	password: <password>
	lat: <latitude>
	lng: <longitude>
	distance: <distance in KM (if distance is missing default 5KM will be used)>
Response(JSON):
{
    "status": 1,
    "errorno": 0,
    "SPOT_IMAGES_BASE_URL": "http://www.ifiego.com/php_ig/searchable-address-irfan/spots/images/",
    "contents": [
        {
            "id": "30",
            "first_name": "Frederique",
            "last_name": "Olivier",
            "phone": "0618920669",
            "email": "ir_gh@yahoo.com",
            "address": "31 Avenue Cap de Croix",
            "city": "Nice",
            "postcode": "06100",
            "access_information": "contact owner",
            "monday_schedule": "09:00-16:00",
            "tuesday_schedule": "09:00-16:00",
            "wednesday_schedule": "00:00-00:00",
            "thursday_schedule": "09:00-16:00",
            "friday_schedule": "09:00-16:00",
            "saturday_schedule": "00:00-00:00",
            "sunday_schedule": "00:00-00:00",
            "longitude": "7.27619980",
            "latitude": "43.72797280",
            "rate_per_hour": "0.08",
            "rate_first_hour": "0.00",
            "status": "active",
            "public": "Yes",
            "need_booking": "Yes",
            "distance": "0.6881349304498415",
            "images": [
                {
                    "title": "testing",
                    "description": "This is a test image",
                    "path": "testtest.jpg"
                }
            ]
        },
        {
            "id": "7",
            "first_name": "Genevieve",
            "last_name": "Asloum",
            "phone": "0619282124",
            "email": "genevieve@test.com",
            "address": "21 boulevard gorbella",
            "city": "nice",
            "postcode": "06000",
            "access_information": "contacter le propietaire",
            "monday_schedule": "09:00-16:00",
            "tuesday_schedule": "09:00-16:00",
            "wednesday_schedule": "09:00-16:00",
            "thursday_schedule": "09:00-16:00",
            "friday_schedule": "09:00-16:00",
            "saturday_schedule": "00:00-00:00",
            "sunday_schedule": "00:00-00:00",
            "longitude": "7.25659830",
            "latitude": "43.71959690",
            "rate_per_hour": "0.05",
            "rate_first_hour": "0.00",
            "status": "active",
            "public": "Yes",
            "need_booking": "Yes",
            "distance": "1.5116169376555442",
            "images": []
        }    ]
}


get Details of spot: (you should not call this api again because you will get all data in above APIs)
URL: /api/provider.php
Required Params(POST key-value):
	method: "detail"
	username: <email>
	password: <password>
	id: <id of spot you previously got>
Response(JSON):
Output will be same as list providers.