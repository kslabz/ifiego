<?php

$includPath = realpath( __DIR__ . '/../includes/');
set_include_path(get_include_path() . PATH_SEPARATOR . $includPath);
require_once ('initialize.php');
header('Content-type: application/json');
