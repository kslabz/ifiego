<?php
require_once 'initAPI.php';
log_request();
$method = trim($_POST ['method']);
$data = array();
$data ['status'] = 0;
$data ['errorno'] = 0;
switch ($method) {
    case 'auth' :
        $username = trim($_POST ['username']);
        $pass = $_POST ['password'];
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            $data ['status'] = STATUS_OK;
            $data ['errorno'] = ERROR_NONE;
            $data ['contents'] = array(
                "user_id" => $user->id,
                "first_name" => $user->first_name,
                "last_name" => $user->last_name,
                "phone" => $user->phone
            );
        }
        break;
        
    case 'signup' :
        $user_data = array();
        $user_data ['password'] = encryptPassword($_POST ['password']);
        $user_data ['first_name'] = trim($_POST ['first_name']);
        $user_data ['last_name'] = trim($_POST ['last_name']);
        $user_data ['email'] = trim($_POST ['email']);
        $user_data ['username'] = trim($_POST ['email']);
        $user_data ['phone'] = trim($_POST ['phone']);
        $alreadyExists = User::isEmailAlreadyExists($user_data ['email']);
        if ($alreadyExists) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_EMAIL_ALREADY_EXIST;
        } else {
            $user = User::instantiate($user_data);
            $user->type = 'user';
            if ($user->save()) {
                $data ['contents'] ['user_id'] = $user->id;
                $data ['status'] = STATUS_OK;
                $data ['errorno'] = ERROR_NONE;
            }
        }
        break;
        
    case 'vehicles' :
        $username = trim($_POST ['username']);
        $pass = $_POST ['password'];
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            $data ['status'] = STATUS_OK;
            $data ['errorno'] = ERROR_NONE;
            $data ['contents'] = User::getUserVehicles($user->id);
            global $VEHICLE_IMAGES_BASE_URL;
            $data['VEHICLE_IMAGES_BASE_URL'] = $VEHICLE_IMAGES_BASE_URL;
        }
        break;
        
    case 'bookings':    
        $username = trim($_POST ['username']);
        $pass = $_POST ['password'];
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            $data ['status'] = STATUS_OK;
            $data ['errorno'] = ERROR_NONE;
            $data ['contents'] = User::getUserBookings($user->id);
        }
        break;
        
    case 'save':
        $username = trim($_POST ['username']);
        $pass = $_POST ['password'];
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            $change_email = false;
            if(isset($_POST['email'])){
                $user->email = trim($_POST['email']);
                $user->username = $user->email;
                $change_email = true;
            }
            if(isset($_POST['phone'])){
                $user->phone = trim($_POST['phone']);
            }
            if(isset($_POST['new_password'])){
                $user->password = encryptPassword($_POST['new_password']);
            }
            if($user->save()){
                $data ['contents'] = array();
                $data ['status'] = STATUS_OK;
                $data ['errorno'] = ERROR_NONE;
            }
            else if($change_email){
                $data ['status'] = STATUS_ERROR;
                $data ['errorno'] = ERROR_EMAIL_ALREADY_EXIST;
            }
        }
        break;
        
    case 'get_spot_rating':
        $username = trim($_POST ['username']);
        $provider_id = trim($_POST ['provider_id']);
        $pass = $_POST ['password'];
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            $data ['status'] = STATUS_OK;
            $data ['errorno'] = ERROR_NONE;
            $data ['contents'] = User::getUserSpotRating($user->id,$provider_id);
        }
        break;
        
    case 'save_spot_rating':
        $username = trim($_POST ['username']);
        $provider_id = trim($_POST ['provider_id']);
        $rating = trim($_POST ['rating']);
        $comments = trim($_POST ['comments']);
        $pass = $_POST ['password'];
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            User::saveUserSpotRating($user->id,$provider_id, $rating, $comments);
            $data ['status'] = STATUS_OK;
            $data ['errorno'] = ERROR_NONE;
        }
        break;
    
    case 'upload_vehicle_image':
        $username = trim($_POST ['username']);
        $pass = $_POST ['password'];
        if(empty($_FILES['image']['name']))
        {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_REQUIRED_PARAM_MISSING;
        }
        else{
            $user = User::authenticate($username, $pass);
            if ($user === false) {
                $data ['status'] = STATUS_ERROR;
                $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
            } else {
                $data ['status'] = STATUS_OK;
                $data ['errorno'] = ERROR_NONE;
                $imgNameParts = explode('.', $_FILES['image']['name']);
                $imgExt = $imgNameParts[count($imgNameParts) - 1];
                $vehicle_id = trim($_POST['vehicle_id']);
                global $ALLOWED_IMAGE_TYPES;
                if(in_array($imgExt, $ALLOWED_IMAGE_TYPES)){
                    $imageName = uniqid() . str_replace(' ','', $imgNameParts[0]) .'.'. $imgExt;
                    global $VEHICLE_IMAGES_PATH;
                    $imagePath = $VEHICLE_IMAGES_PATH.$imageName;
                    if(move_uploaded_file($_FILES['image']['tmp_name'], $imagePath)){
                        User::saveVehicleImage($imageName, $user->id, $vehicle_id);
                    }else{
                        $data ['status'] = STATUS_ERROR;
                        $data ['errorno'] = $_FILES['image']['error'];
                    }
                }
                else{
                    $data ['status'] = STATUS_ERROR;
                    $data ['errorno'] = ERROR_REQUIRED_PARAM_MISSING;
                }
            }
        }
        break;
    case 'add_vehicle':
        $username = trim($_POST ['username']);
        $pass = $_POST ['password'];
        if(empty($_FILES['image']['name']))
        {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_REQUIRED_PARAM_MISSING;
            //log_debug("add_vehicl", json_encode($_FILES));
        }
        else{
            //log_debug("add_vehicl", "User authenticate");
            $user = User::authenticate($username, $pass);
            if ($user === false) {
                $data ['status'] = STATUS_ERROR;
                $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
            } else {
                $data ['status'] = STATUS_OK;
                $data ['errorno'] = ERROR_NONE;
                $registraion  = $_POST['registration'];
                $type = $_POST['type'];
                //$carID = $_POST['carID'];
                $imgNameParts = explode('.', $_FILES['image']['name']);
                $imgExt = $imgNameParts[count($imgNameParts) - 1];
                //$vehicle_id = trim($_POST['vehicle_id']);
                global $ALLOWED_IMAGE_TYPES;
                //log_debug("add_vehicl", json_encode($ALLOWED_IMAGE_TYPES));
                //log_debug("add_vehicl", $imgExt);
                //log_debug("add_vehicl", json_encode($imgNameParts));
                if(in_array($imgExt, $ALLOWED_IMAGE_TYPES)){
                    $imageName = uniqid() . str_replace(' ','', $imgNameParts[0]) .'.'. $imgExt;
                    global $VEHICLE_IMAGES_PATH;
                    $imagePath = $VEHICLE_IMAGES_PATH.$imageName;
                    //log_debug("add_vehicl", $imagePath);
                    if(move_uploaded_file($_FILES['image']['tmp_name'], $imagePath)){
                        $data ['contents'] = User::addVehicle($user->id, $registraion, $type, '', $imageName);
                    }else{
                        log_debug("add_vehicle", $_FILES['image']['error']);
                        $data ['status'] = STATUS_ERROR;
                        $data ['errorno'] = $_FILES['image']['error'];
                    }
                }
                else{
                    $data ['status'] = STATUS_ERROR;
                    $data ['errorno'] = ERROR_REQUIRED_PARAM_MISSING;
                }
            }
        }
        break;
    
    case 'new_booking':
        $username = trim($_POST ['username']);
        $pass = $_POST['password'];
        $provider_id = trim($_POST ['provider_id']);
        $vehicle_id = trim($_POST ['vehicle_id']);
        $timestart = trim($_POST ['from']);
        $timeend = $_POST ['to'];
        $fee = $_POST['fee'];
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            $data['contents'] = User::addNewUserBooking($user->id,$provider_id, $vehicle_id, $timestart, $timeend, $fee);
            $data ['status'] = STATUS_OK;
            $data ['errorno'] = ERROR_NONE;
        }
        break;
    
    case 'active_booking':
        $username = trim($_POST ['username']);
        $pass = $_POST['password'];
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            $active_booking = User::getActiveBooking($user->id);
            $data ['contents'] = $active_booking;
            $data ['status'] = STATUS_OK;
            $data ['errorno'] = ERROR_NONE;
        }
        break;
        
    case 'open_gate':
        $username = trim($_POST ['username']);
        $pass = $_POST['password'];
        $provider_id = trim($_POST ['provider_id']);
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            if(User::openGate($user->id,$provider_id))
            {
                $data ['status'] = STATUS_OK;
                $data ['errorno'] = ERROR_NONE;
            }else{
                
            }
        }
        break;
    
    case 'check_paypal':
        $username = trim($_POST ['username']);
        $pass = $_POST['password'];
        
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            $data ['contents'] = User::isPaypalExists($user->id);
            $data ['status'] = STATUS_OK;
            $data ['errorno'] = ERROR_NONE;
        }

        
        break;
    
    case 'add_paypal':
        require_once 'PaypalHelper.php';
        $username = trim($_POST ['username']);
        $pass = $_POST['password'];
        if(empty($_POST['access_token']))
        {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_REQUIRED_PARAM_MISSING;
        }else{
            $user = User::authenticate($username, $pass);
            if ($user === false) {
                $data ['status'] = STATUS_ERROR;
                $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
            } else {
                log_debug('paypal-mobile-response', $_POST['access_token']);
                $paypal_response = json_decode($_POST['access_token'], true);
                log_debug("paypal-response-printr", print_r($paypal_response, true));
                $correlation_id = $_POST['correlation_id'];
                $data ['contents'] = User::getAndSaveRefreshToken($user->id, $paypal_response['response']['code'], $correlation_id);
                $data ['status'] = STATUS_OK;
                $data ['errorno'] = ERROR_NONE;
            }
        }
        break;
        
        
    case 'forget_password':
        $email = filter_input(INPUT_POST,"email",FILTER_VALIDATE_EMAIL);
        if($email){
            $sent = User::resetPassword($email);
            if($sent){
                $data ['status'] = STATUS_OK;
                $data ['errorno'] = ERROR_NONE;
            }else{
                $data ['status'] = STATUS_ERROR;
                $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
            }
        }else{
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_REQUIRED_PARAM_MISSING;
        }           
            
            
        break;
        
        
    default :
        $data ['status'] = STATUS_ERROR;
        $data ['errorno'] = ERROR_NO_API_METHOD;
}
$response = json_encode($data);
log_debug("user-response", $response, "info");
echo $response;
?>