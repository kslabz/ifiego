<?php
require_once 'initAPI.php';
log_request();

$method = trim($_POST ['method']);
$data = array();
$data ['status'] = 0;
$data ['errorno'] = 0;
switch ($method)
{
case 'list':
    //latitude
    if (isset($_GET['lat'])) {
        $latitude = $_GET['lat'];
    } else if (isset($_POST['lat'])) {
        $latitude = $_POST['lat'];
    }

    //longitude
    if (isset($_GET['lng'])) {
        $longitude = $_GET['lng'];
    } else if (isset($_POST['lng'])) {
        $longitude = $_POST['lng'];
    }
    //X - km
    if (isset($_GET['distance'])) {
        $distance = $_GET['distance'];
    } else if (isset($_POST['distance'])) {
        $distance = $_POST['distance'];
    }
    if(empty($distance))
    {
        $distance = DEFAULT_DISTANCE_SEARCH;
    }
    if (isset($latitude) && isset($longitude)){
        $username = trim($_POST ['username']);
        $pass = $_POST ['password'];
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            $data ['status'] = STATUS_OK;
            $data ['errorno'] = ERROR_NONE;
            global $SPOT_IMAGES_BASE_URL;
            $data['SPOT_IMAGES_BASE_URL'] = $SPOT_IMAGES_BASE_URL;
            $data ['contents'] = Provider::findAllByDistance($latitude, $longitude, $distance);
        }

    }else{
        $data ['status'] = STATUS_ERROR;
        $data ['errorno'] = ERROR_REQUIRED_PARAM_MISSING;
    }
    break;
    
case 'search':
    if (isset($_POST['address'])) {
        $address = $_POST['address'];
    }
    if (isset($address)){
        $username = trim($_POST ['username']);
        $pass = $_POST ['password'];
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            $data ['status'] = STATUS_OK;
            $data ['errorno'] = ERROR_NONE;
            $data ['contents'] = Provider::findAllByAddress($address);
        }

    }else{
        $data ['status'] = STATUS_ERROR;
        $data ['errorno'] = ERROR_REQUIRED_PARAM_MISSING;
    }
    break;
    
case 'detail':
    if (isset($_POST['id'])) {
        $povider_id = $_POST['id'];
    }
    if (isset($povider_id)){
        $username = trim($_POST ['username']);
        $pass = $_POST ['password'];
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            $data ['status'] = STATUS_OK;
            $data ['errorno'] = ERROR_NONE;
            global $SPOT_IMAGES_BASE_URL;
            $data['SPOT_IMAGES_BASE_URL'] = $SPOT_IMAGES_BASE_URL;
            $data ['contents'] = Provider::getDetailsOfSpot($povider_id);
        }

    }else{
        $data ['status'] = STATUS_ERROR;
        $data ['errorno'] = ERROR_REQUIRED_PARAM_MISSING;
    }
    break;

case 'spot_avg_rating':
    if (isset($_POST['id'])) {
        $provider_id = $_POST['id'];
    }
    if (isset($provider_id)){
        $username = trim($_POST ['username']);
        $pass = $_POST ['password'];
        $user = User::authenticate($username, $pass);
        if ($user === false) {
            $data ['status'] = STATUS_ERROR;
            $data ['errorno'] = ERROR_USER_PASS_MISMATCH;
        } else {
            $data ['status'] = STATUS_OK;
            $data ['errorno'] = ERROR_NONE;
            //global $SPOT_IMAGES_BASE_URL;
            //$data['SPOT_IMAGES_BASE_URL'] = $SPOT_IMAGES_BASE_URL;
            $cont1 = Provider::getAvgSpotRating($provider_id);
            $cont2 = Provider::getDetailsOfSpot($provider_id);
            $cont3 = User::getUserSpotRating($user->id, $provider_id);
            if(empty($cont2[0])){
                $cont3[0]['comments'] = '';
            }
            $cont3[0]['rating'] = $cont1[0]['rating'];
            if(!empty($cont2[0]['first_name'])){
                $cont3[0]['first_name'] = $cont2[0]['first_name'];
                $cont3[0]['last_name'] = $cont2[0]['last_name'];
                $cont3[0]['address'] = $cont2[0]['address'];
                $cont3[0]['public'] = $cont2[0]['public'];
            }
            $data ['contents'] = $cont3;
        }

    }else{
        $data ['status'] = STATUS_ERROR;
        $data ['errorno'] = ERROR_REQUIRED_PARAM_MISSING;
    }
    break;
    
default :
    $data ['status'] = STATUS_ERROR;
    $data ['errorno'] = ERROR_NO_API_METHOD;
    break;
}
$response = json_encode($data);
log_debug("provider-response", $response, "info");
echo $response;
/*  
  if (isset($latitude) && isset($longitude) && isset($x)) {
  $x = (float) $x;
  if ($x > 0) {
  //select all providers
  $query ="SELECT * FROM `providers`";
  $rsProviders = mysql_query($query, $db_webservice) or
  die(mysql_error());

  $latArray = array();
  $longArray = array();
  $keys = array();
  while ($row_rsProviders = mysql_fetch_assoc($rsProviders)) {//keep longitude and latitude of all providers in arrays
  if ($row_rsProviders['latitude'] != 0 && $row_rsProviders['longitude'] !=0 && $row_rsProviders['status'] == 'active'){
  $latArray[] = $row_rsProviders['latitude'];
  $longArray[] = $row_rsProviders['longitude'];
  $keys[] = $row_rsProviders['id'];
  }
  }

  $finalKeys = array();
  $distance = array();
  foreach ($latArray as $key => $val) {//calculate distance => store only the ones with distance < $x
  $distance = haversineGreatCircleDistance($latArray[$key], $longArray[$key], $latitude, $longitude) / 1000; // km
  if ($distance <= $x) {
  $finalKeys[] = $keys[$key];
  }
  }

  $arRows = array();
  if (!empty($finalKeys)) { // final query
  $finalQuery ="SELECT * FROM `providers` WHERE `id` IN ('" . implode("','", $finalKeys) . "')";
  $finalProviders = mysql_query($finalQuery, $db_webservice) or die(mysql_error());
  while ($row_rsProviders = mysql_fetch_assoc($finalProviders)) {
  unset($row_rsProviders['id']);
  array_push($arRows, $row_rsProviders);
  }
  }

  header('Content-type: application/json');
  echo json_encode($arRows);
  }
  else {
  echo 'X must be a positive number.';
  }
  }
  else {
  echo 'Missing parameters.';
  }

 */
?>
