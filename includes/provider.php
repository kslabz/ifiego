<?php
// If it's going to need the database, then it's
// probably smart to require it before we start.
require_once (LIB_PATH . DS . 'database.php');

class Provider{

    protected static $table_name = "providers";
    protected static $db_fields = array(
        'id',
        'first_name',
        'last_name',
        'phone',
        'email',
        'address',
        'city',
        'postcode',
        'access_information',
        'monday_schedule',
        'tuesday_schedule',
        'wednesday_schedule',
        'thursday_schedule',
        'friday_schedule',
        'saturday_schedule',
        'sunday_schedule',
        'longitude',
        'latitude',
        'rate_per_hour',
        'rate_first_hour',
        'rate_per_day',
        'rate_first_day',
        'need_booking',
        'public',
        'full_day',
        'status'
            )
    ;
    public $id;
    public $first_name;
    public $last_name;
    public $phone;
    public $email;
    public $address;
    public $city;
    public $postcode;
    public $access_information;
    public $monday_schedule;
    public $tuesday_schedule;
    public $wednesday_schedule;
    public $thursday_schedule;
    public $friday_schedule;
    public $saturday_schedule;
    public $sunday_schedule;
    public $longitude;
    public $latitude;
    public $rate_per_hour;
    public $rate_first_hour;
    public $rate_per_day;
    public $rate_first_day;
    public $need_booking;
    public $public;
    public $full_day;
    public $status;

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    private static $instance = NULL;

    static public function getInstance() {
        if (is_null(self::$instance)) {

            self::$instance = new self ();
        }

        return self::$instance;
    }

    // Common Database Methods
    public static function find_all() {
        return self::find_by_sql("SELECT * FROM " . self::$table_name);
    }

    public static function find_by_id($id = 0) {
        global $database;

        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE id=" . $database->escape_value($id) . " LIMIT 1");

        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_sql($sql = "") {
        global $database;

        $result_set = $database->query($sql);

        $object_array = array();

        while ($row = $database->fetch_array($result_set)) {

            $object_array [] = self::instantiate($row);
        }

        return $object_array;
    }

    public static function count_all() {
        global $database;

        $sql = "SELECT COUNT(*) FROM " . self::$table_name;

        $result_set = $database->query($sql);

        $row = $database->fetch_array($result_set);

        return array_shift($row);
    }

    private static function instantiate($record) {

        // Could check that $record exists and is an array
        $object = new self ();

        // Simple, long-form approach:
        // $object->id = $record['id'];
        // $object->username = $record['username'];
        // $object->password = $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name = $record['last_name'];
        // More dynamic, short-form approach:

        foreach ($record as $attribute => $value) {

            if ($object->has_attribute($attribute)) {

                $object->$attribute = $value;
            }
        }

        return $object;
    }

    private function has_attribute($attribute) {

        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {

        // return an array of attribute names and their values
        $attributes = array();

        foreach (self::$db_fields as $field) {

            if (property_exists($this, $field)) {

                $attributes [$field] = $this->$field;
            }
        }

        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;

        $clean_attributes = array();

        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute

        foreach ($this->attributes() as $key => $value) {

            $clean_attributes [$key] = $database->escape_value($value);
        }

        return $clean_attributes;
    }

    public function save() {

        // A new record won't have an id yet.
        return isset($this->id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;

        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection

        $attributes = $this->sanitized_attributes();

        $sql = "INSERT INTO " . self::$table_name . " (";

        $sql .= join(", ", array_keys($attributes));

        $sql .= ") VALUES ('";

        $sql .= join("', '", array_values($attributes));

        $sql .= "')";

        if ($database->query($sql)) {

            $this->id = $database->insert_id();

            return true;
        } else {

            return false;
        }
    }

    public function update() {
        global $database;

        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection

        $attributes = $this->sanitized_attributes();

        $attribute_pairs = array();

        foreach ($attributes as $key => $value) {

            $attribute_pairs [] = "{$key}='{$value}'";
        }

        $sql = "UPDATE " . self::$table_name . " SET ";

        $sql .= join(", ", $attribute_pairs);

        $sql .= " WHERE id=" . $database->escape_value($this->id);

        $database->query($sql);

        return ($database->affected_rows() == 1) ? true : false;
    }

    public function delete() {
        global $database;

        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1

        $sql = "DELETE FROM " . self::$table_name;

        $sql .= " WHERE id=" . $database->escape_value($this->id);

        $sql .= " LIMIT 1";

        $database->query($sql);

        return ($database->affected_rows() == 1) ? true : false;

        // NB: After deleting, the instance of User still
        // exists, even though the database entry does not.
        // This can be useful, as in:
        // echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update()
        // after calling $user->delete().
    }
    static function findBySql($sql)
    {
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = mysql_fetch_assoc($result_set)) {
            $object_array [] = $row;
        }
        return $object_array;
    }
    public static function findAllByDistance($latitude, $longitude, $distance, $limit=20, $offset=0) {
        /*$lat1 = ;
        $lat2 = ;
        $lng1 = ;
        $lng2 = ;*/
        $sql = "SELECT *, ( 6371 * acos( cos( radians($latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) 
            - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( latitude ) ) ) ) 
            AS distance FROM providers
            WHERE status = 'active'
            HAVING distance < $distance ORDER BY distance LIMIT $offset , $limit";
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = mysql_fetch_assoc($result_set)) {
            $row['images'] = Provider::getImagesByProvider($row['id']);
            $row['bookings'] = Provider::getProviderFutureBookings($row['id'], $row['full_day']);
            $object_array [] = $row;
        }
        return $object_array;
    }
    static function getImagesByProvider($provider_id){
        $sql = "SELECT title, description, path FROM provider_images WHERE provider_id = '$provider_id' LIMIT 10";
        return Provider::findBySql($sql);
    }
    public static function findAllByAddress($address, $limit=20, $offset=0)
    {
        $address = mysql_real_escape_string($address);
        $sql = "SELECT * FROM providers
            WHERE status = 'active' AND address LIKE '%$address%'
            LIMIT $offset , $limit";
        return self::find_by_sql($sql);
    }
    public static function getDetailsOfSpot($id)
    {
        $id = mysql_real_escape_string($id);
        $sql = "SELECT * FROM providers
            WHERE id = '$id' AND status = 'active'";
        $provider = Provider::findBySql($sql);
        if(!empty($provider)){
            $provider[0]['images'] = Provider::getImagesByProvider($id);
            $provider[0]['bookings'] = Provider::getProviderFutureBookings($provider[0]['id'], $provider[0]['full_day']);
        }
        return $provider;
    }
    public static function getAvgSpotRating($id)
    {
        $id = mysql_real_escape_string($id);
        $sql = "SELECT ROUND(IFNULL(AVG(rating), 0), 2) AS 'rating' FROM spot_ratings
            WHERE provider_id = '$id'";
        $provider = Provider::findBySql($sql);
        return $provider;
    }
    
    public static function saveImageForProvider($imagePath, $provideID)
    {
        $sql = "INSERT INTO provider_images (path, provider_id) VALUES('$imagePath', '$provideID')" ;
        global $database;
        $database->query($sql);
    }
    public static function getProviderBookings($provider_id){
        //$sql = "SELECT * FROM bookings WHERE provider_id=$provider_id AND NOW() BETWEEN start_datetime AND end_datetime ";
        $sql = "SELECT * FROM bookings WHERE provider_id=$provider_id ORDER BY timestamp DESC";
        return Provider::findBySql($sql);
    }
    public static function getProviderFutureBookings($provider_id, $full_day){
        $interval = '';
        if($full_day == 'Yes'){
           $interval =  '1 MONTH';
        }else{
            $interval =  '1 DAY';
        }
        $sql = "SELECT start_datetime, end_datetime FROM bookings "
            . " WHERE provider_id=$provider_id AND start_datetime < (NOW() + INTERVAL $interval ) AND end_datetime > NOW() "
            . " ORDER BY start_datetime ASC";
        log_debug("Provider:QUERY:$interval", $sql);
        return Provider::findBySql($sql);
    }
}
?>