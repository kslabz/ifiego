<?php
define('STATUS_OK', 1);
define('STATUS_ERROR', 0);

define('ERROR_NONE', 0);
define('ERROR_NO_API_METHOD', 1);
define('ERROR_USERNAME_EMPTY', 2);
define('ERROR_PASSWORD_EMPTY', 3);
define('ERROR_EMAIL_EMPTY', 4);
define('ERROR_USER_PASS_MISMATCH', 5);
define('ERROR_USER_NOT_AUTHORIZED', 6);
define('ERROR_EMAIL_ALREADY_EXIST', 7);
define('ERROR_REQUIRED_PARAM_MISSING', 8);

define("DEFAULT_DISTANCE_SEARCH", 5);

global $ALLOWED_IMAGE_TYPES;
$ALLOWED_IMAGE_TYPES = array('jpg', 'jpeg', 'png', 'gif');
global $base_url;
$base_url = 'http://www.ifiego.com/php_ig/searchable-address-irfan/';
//$base_url = 'http://'.$_SERVER['SERVER_NAME'].'/ifiego/';
global $SPOT_IMAGES_BASE_URL;
$SPOT_IMAGES_BASE_URL = $base_url.'spots/images/';
global $SPOT_IMAGES_PATH;
$SPOT_IMAGES_PATH = __DIR__ . '/../spots/images/';
global $VEHICLE_IMAGES_BASE_URL;
$VEHICLE_IMAGES_BASE_URL = $base_url.'user/vehicle/';
global $VEHICLE_IMAGES_PATH;
$VEHICLE_IMAGES_PATH = __DIR__ . '/../user/vehicle/';

global $EMAIL_FROM_SUPPORT;
$EMAIL_FROM_SUPPORT = 'support@ifiego.com'
?>