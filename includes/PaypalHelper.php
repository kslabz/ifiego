<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'paypal/vendor/autoload.php';
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Auth\Openid\PPOpenIdTokeninfo;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;


class PaypalHelper {

    static $clientID = 'AcsfZxCTKCNvaG2sx0iHxa88Jv5ABQ_V1iFfXwDO-v4scgeb6E02CPVp1Rp7';
    static $secretKey = 'EAi2YxAb4TG7SpesDRqZ5H2I_b7-lYZ_09ATeNkhosEmoqxJmHIZ17qCiUyC';
    
    static function getAPIContextConfig(){
        return  $apiContextConfig = array(
        'mode' => 'sandbox',
        'http.ConnectionTimeOut' => 30,
        'log.LogEnabled' => true,
        'log.FileName' => __DIR__.'/../logs/PayPal.log',
        'log.LogLevel' => 'FINE'
    );
    }
    public static function getAccessTokenFromRefreshToken($refresh_token){
        $params = array(
            'refresh_token' => $refresh_token,
			'client_id' => PaypalHelper::$clientID,
			'client_secret' => PaypalHelper::$secretKey,
            'grant_typ'=>'authorization_code',
		);
        $accessToken = '';
        try{
            $apiContext = PaypalHelper::getApiContext();
            $accessToken = PPOpenIdTokeninfo::createFromRefreshToken($params, $apiContext);
            log_debug("debug-paypal", json_encode($accessToken));
            
        }  catch (Exception $ex){
            $ex_print = print_r($ex->getData(), true);
            log_debug("debug-paypal-exception", $ex_print);
        }        
        return $accessToken;
    }

    public static function getRefreshToken($access_token) {
        $params = array(
			'code' => $access_token,
			//'redirect_uri' => 'urn:ietf:wg:oauth:2.0:oob',
			'client_id' => PaypalHelper::$clientID,
			'client_secret' => PaypalHelper::$secretKey,
            'grant_typ'=>'authorization_code',
            //'response_type'=>'token'
		);
        $refreshToken = '';
        try{
            $apiContext = PaypalHelper::getApiContext();
            $accessToken = PPOpenIdTokeninfo::createFromAuthorizationCode($params, $apiContext);
            log_debug("debug-paypal", json_encode($accessToken));
            $refreshToken = $accessToken->getRefreshToken();
            log_debug("debug-paypal", $refreshToken);
        }  catch (Exception $ex){
            $ex_print = print_r($ex->getData(), true);
            log_debug("debug-paypal-exception", $ex_print);
        }        
        return $refreshToken;
    }
    static public function executePayment($refresh_token, $correlation_id, $fee_amount)
    {
        $apiContext = PaypalHelper::getApiContext();
        
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");
        
        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($fee_amount);
        //    ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
        //    ->setItemList($itemList)
            ->setDescription("Payment description");
        
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl("http://localhost/ExecutePayment.php?success=true")
            ->setCancelUrl("http://localhost/ExecutePayment.php?success=false");
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));
        $accessToken = PaypalHelper::getAccessTokenFromRefreshToken($refresh_token);
        $token = $accessToken->getAccessToken();
        try {
            $payment->create($apiContext);
            log_debug("PaypalPayment Success", print_r($payment, true));
            $paymentExecution = new PayPal\Api\PaymentExecution;
            $payment->execute($paymentExecution, $apiContext);            
        } catch (PayPal\Exception\PPConnectionException $ex) {
            log_debug("PaypalPaymentException: " . $ex->getMessage(), print_r($ex->getData(), true));
            return false;
        }
    }

    
    


    /**
     * Helper method for getting an APIContext for all calls
     *
     * @return PayPal\Rest\ApiContext
     */
    static function getApiContext() {

        // ### Api context
        // Use an ApiContext object to authenticate 
        // API calls. The clientId and clientSecret for the 
        // OAuthTokenCredential class can be retrieved from 
        // developer.paypal.com

        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                PaypalHelper::$clientID,
                PaypalHelper::$secretKey
            )
        );



        // #### SDK configuration
        // Comment this line out and uncomment the PP_CONFIG_PATH
        // 'define' block if you want to use static file 
        // based configuration

        $apiContext->setConfig(
            PaypalHelper::getAPIContextConfig()
        );

        /*
          // Register the sdk_config.ini file in current directory
          // as the configuration source.
          if(!defined("PP_CONFIG_PATH")) {
          define("PP_CONFIG_PATH", __DIR__);
          }
         */

        return $apiContext;
    }

}

