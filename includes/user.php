<?php

// If it's going to need the database, then it's
// probably smart to require it before we start.
require_once (LIB_PATH . DS . 'database.php');

class User extends Database {

    protected static $table_name = "users";
    protected static $db_fields = array(
        'id',
        'username',
        'password',
        'first_name',
        'last_name',
        'phone',
        'email',
        'type',
        'status',
    );
    public $id;
    public $username;
    public $password;
    public $first_name;
    public $last_name;
    public $phone;
    public $email;
    public $type;
    public $status;
    private static $instance = NULL;

    static public function getInstance() {
        if (is_null(self::$instance)) {

            self::$instance = new self ();
        }

        return self::$instance;
    }

    public function full_name() {
        if (isset($this->first_name) && isset($this->last_name)) {

            return $this->first_name . " " . $this->last_name;
        } else {

            return "";
        }
    }

    private static function password_check($password, $existing_hash) {

        // existing hash contains format and salt at start
        $hash = crypt($password, $existing_hash);

        // echo $hash.'<br>';
        // echo $existing_hash; exit();

        if ($hash === $existing_hash) {

            return true;
        } else {

            return false;
        }
    }

    public static function authenticate($username = "", $password = "", $admin=false) {
        global $database;

        $username = $database->escape_value($username);

        $password = $database->escape_value($password);

        $sql = "SELECT * FROM users ";

        $sql .= "WHERE username = '{$username}' ";
        if($admin){
           $sql .= " AND type='admin' ";
        }
        else{
            $sql .= " AND type='user' ";
        }
        $sql .= " AND status ='active' LIMIT 1";

        $result_array = self::find_by_sql($sql);

        // return !empty($result_array) ? array_shift($result_array) : false;

        if (!empty($result_array)) {

            if (self::password_check($password, $result_array [0]->password)) {

                // password matches

                return array_shift($result_array);
            } else {

                // password does not match

                return false;
            }
        } else {

            // admin not found

            return false;
        }
    }

    // Common Database Methods
    public static function find_all() {
        return self::find_by_sql("SELECT * FROM " . self::$table_name);
    }

    public static function find_by_id($id = 0) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE id={$id} LIMIT 1");

        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_sql($sql = "") {
        global $database;

        $result_set = $database->query($sql);

        $object_array = array();

        while ($row = $database->fetch_array($result_set)) {

            $object_array [] = self::instantiate($row);
        }

        return $object_array;
    }

    public static function count_all() {
        global $database;

        $sql = "SELECT COUNT(*) FROM " . self::$table_name;

        $result_set = $database->query($sql);

        $row = $database->fetch_array($result_set);

        return array_shift($row);
    }

    public static function instantiate($record) {

        // Could check that $record exists and is an array
        $object = new self ();

        // Simple, long-form approach:
        // $object->id = $record['id'];
        // $object->username = $record['username'];
        // $object->password = $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name = $record['last_name'];
        // More dynamic, short-form approach:

        foreach ($record as $attribute => $value) {

            if ($object->has_attribute($attribute)) {

                $object->$attribute = $value;
            }
        }

        return $object;
    }

    private function has_attribute($attribute) {

        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {

        // return an array of attribute names and their values
        $attributes = array();

        foreach (self::$db_fields as $field) {

            if (property_exists($this, $field)) {

                $attributes [$field] = $this->$field;
            }
        }

        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;

        $clean_attributes = array();

        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute

        foreach ($this->attributes() as $key => $value) {

            $clean_attributes [$key] = $database->escape_value($value);
        }

        return $clean_attributes;
    }

    public function save() {

        // A new record won't have an id yet.
        return isset($this->id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;

        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection

        $attributes = $this->sanitized_attributes();

        $sql = "INSERT INTO " . self::$table_name . " (";

        $sql .= join(", ", array_keys($attributes));

        $sql .= ") VALUES ('";

        $sql .= join("', '", array_values($attributes));

        $sql .= "')";

        if ($database->query($sql)) {

            $this->id = $database->insert_id();

            return true;
        } else {

            return false;
        }
    }

    public function update() {
        global $database;

        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection

        $attributes = $this->sanitized_attributes();

        $attribute_pairs = array();

        foreach ($attributes as $key => $value) {

            $attribute_pairs [] = "{$key}='{$value}'";
        }

        $sql = "UPDATE IGNORE " . self::$table_name . " SET ";

        $sql .= join(", ", $attribute_pairs);

        $sql .= " WHERE id=" . $database->escape_value($this->id);

        $database->query($sql);

        return ($database->affected_rows() == 1) ? true : false;
    }

    public function delete() {
        global $database;

        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1

        $sql = "DELETE FROM " . self::$table_name;

        $sql .= " WHERE id=" . $database->escape_value($this->id);

        $sql .= " LIMIT 1";

        $database->query($sql);

        return ($database->affected_rows() == 1) ? true : false;

        // NB: After deleting, the instance of User still
        // exists, even though the database entry does not.
        // This can be useful, as in:
        // echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update()
        // after calling $user->delete().
    }
    public static function findBySql($sql){
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = mysql_fetch_assoc($result_set)) {
            $object_array [] = $row;
        }
        return $object_array;
    }
    static function executeSql($sql){
        global $database;
        return $database->query($sql);
    }
    public static function findByAttribute($attribute, $value) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE `{$attribute}`='{$value}' LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function isEmailAlreadyExists($email) {
        $user = User::findByAttribute("email", $email);
        return !empty($user);
    }
    public static function getUserVehicles($user_id){
        $sql = "SELECT id,registration, image_path, type FROM vehicles WHERE user_id=$user_id";
        return User::findBySql($sql);
    }
    public static function getVehicle($vhcl_id){
        $sql = "SELECT registration, image_path, type FROM vehicles WHERE id='$vhcl_id'";
        return User::findBySql($sql);
    }
    public static function getUserBookings($user_id){
        $sql = "SELECT sp.*, p.address FROM bookings sp JOIN providers p ON p.id = sp.provider_id WHERE sp.user_id=$user_id ORDER BY sp.timestamp DESC";
        return User::findBySql($sql);
    }
    public static function addNewUserBooking($user_id,$provider_id, $vehicle_id, $timestart, $timeend, $fee){
        $sql = "INSERT INTO bookings (user_id, provider_id, vehicle_id, start_datetime, end_datetime, price)"
            . " VALUES('$user_id', '$provider_id', '$vehicle_id', '$timestart', '$timeend', '$fee')";
        global $database;
        User::executeSql($sql);
        User::sendBookingConfirmation($user_id, $provider_id);
        return array('id'=>$database->insert_id());
    }
    static function sendBookingConfirmation($user_id, $provider_id){
        $provider = Provider::find_by_id($provider_id);
        $user = User::find_by_id($user_id);
        //sendMail($to, $from, $subject, $message);
        //sendSMS($to, $sms);
    }
    public static function openGate($user_id,$provider_id)
    {
        return true;
    }
    public static function getUserSpotRating($user_id, $provider_id){
        $sql = "SELECT sp.rating, sp.comments, p.first_name, p.last_name, p.address, p.public FROM spot_ratings sp JOIN providers p ON p.id = sp.provider_id WHERE sp.user_id=$user_id AND sp.provider_id='$provider_id'";
        return User::findBySql($sql);
    }
    public static function saveUserSpotRating($user_id,$provider_id, $rating, $comments){
        $sql = "INSERT INTO spot_ratings (user_id, provider_id, rating, comments)"
            . " VALUES('$user_id','$provider_id','$rating','$comments')"
            . " ON DUPLICATE KEY UPDATE rating='$rating', comments='$comments'";
        return User::executeSql($sql);
    }
    public static function saveVehicleImage($imageName, $user_id, $vehicle_id){
        $sql = "UPDATE vehicles SET image_path='$imageName' WHERE id=$vehicle_id AND user_id=$user_id";
        return User::executeSql($sql);
    }
    public static function updateVehicle($user_id, $vehicle_id, $registraion, $type){ //update
        $sql = "UPDATE vehicles SET registration='$registraion', type='$type' WHERE id=$vehicle_id AND user_id=$user_id";
        return User::executeSql($sql);
    }
    public static function addVehicle($user_id, $registraion, $type, $carID, $imageName){
        //$sql = "UPDATE vehicles SET registration='$registraion', type='$type' WHERE id=$vehicle_id AND user_id=$user_id";
        $sql = "INSERT INTO vehicles (user_id, registration, type, carID, image_path) VALUES('$user_id', '$registraion', '$type', '$carID', '$imageName')";
        global $database;
        User::executeSql($sql);
        return array('id'=>$database->insert_id());
    }
    public static function delVehicle($vehicle_id){
        $sql = "DELETE FROM vehicles WHERE id=$vehicle_id";
        User::executeSql($sql);
        global $database;
        return $database->affected_rows();
    }
    public static function getAllBookings()
    {
        $sql = "SELECT b.*, s.first_name as s_first_name, s.last_name as s_last_name, u.first_name as u_first_name, u.last_name as u_last_name"
            . " FROM bookings b "
            . " JOIN users u ON u.id = b.user_id JOIN providers s ON s.id = b.provider_id"
            . " ORDER BY timestamp DESC ";
        return User::findBySql($sql);
    }
    public static function getActiveBooking($user_id)
    {
        $sql = "SELECT b.*, p.address FROM bookings b JOIN providers p ON p.id = b.provider_id WHERE b.user_id = '$user_id' AND NOW() BETWEEN b.start_datetime AND b.end_datetime LIMIT 1";
        //log_debug("ACTIVEBOOKING: QUERY", $sql);
        return User::findBySql($sql);
    }
    public static function getAndSaveRefreshToken($user_id, $access_token, $correlation_id=''){
        $refresh_token = PaypalHelper::getRefreshToken($access_token);
        if($refresh_token){
            $sql = "INSERT INTO user_paypal_details (user_id, refresh_token, correlation_id) VALUES('$user_id', '$refresh_token', '$correlation_id') "
                . " ON DUPLICATE KEY UPDATE refresh_token='$refresh_token', correlation_id='$correlation_id'";
            User::executeSql($sql);
            return mysql_affected_rows() > 0;
        }
        return false;
    }
    public static function isPaypalExists($user_id){
        $sql = "SELECT COUNT(*) AS 'count' FROM user_paypal_details WHERE user_id='$user_id'";
        return User::findBySql($sql);
    }
    public static function resetPassword($email){
        $new_password = uniqid();
        $enc_pass = encryptPassword($new_password);
        $sql = "UPDATE users SET password='$enc_pass' WHERE email='$email'";
        global $database;
        User::executeSql($sql);
        $affected = $database->affected_rows();
        if($affected == 1){
            $message = "Hi, \n <br> You have requested to reset your password. Here is your password. \n <br> {$new_password} \n <br> Best Wishes,\n <br> IFIEGO Support Team";
            $sent = sendMail($email, "support@ifiego.com", "Password Reset Request", $message);
            if($sent){
               return true; 
            }
            
        }
        return $affected == 1;
    }
    
    public function getAllAdminUsers(){
        $sql = "SELECT * FROM users WHERE type='admin'";
        return self::find_by_sql($sql);
    }
    public function getAllStandardUsers(){
        $sql = "SELECT * FROM users WHERE type='user'";
        return self::find_by_sql($sql);
    }
}
?>
