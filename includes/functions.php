<?php

function strip_zeros_from_date($marked_string = "") {
    // first remove the marked zeros
    $no_zeros = str_replace('*0', '', $marked_string);
    // then remove any remaining marks
    $cleaned_string = str_replace('*', '', $no_zeros);

    return $cleaned_string;
}

function redirect_to($location = null) {
    if ($location != null) {
        header("Location: {$location}");
        exit;
    }
}

function output_message($message = "") {
    if (!empty($message)) {
        return "<p class=\"message\">{$message}</p>";
    } else {
        return "";
    }
}

function __autoload($class_name) {
    $class_name = strtolower($class_name);
    $path = LIB_PATH . DS . "{$class_name}.php";
    if (file_exists($path)) {
        require_once($path);
    } else {
        die("The file {$class_name}.php could not be found.");
    }
}

function include_layout_template($template = "") {
    include(SITE_ROOT . DS . 'layouts' . DS . $template);
}

function log_action($action, $message = "") {
    $logfile = SITE_ROOT . DS . 'logs' . DS . 'log.txt';
    $new = file_exists($logfile) ? false : true;
    if ($handle = fopen($logfile, 'a')) { // append
        $timestamp = strftime("%Y-%m-%d %H:%M:%S", time());
        $content = "{$timestamp} | {$action}: {$message}\n";
        fwrite($handle, $content);
        fclose($handle);
        if ($new) {
            chmod($logfile, 0755);
        }
    } else {
        echo "Could not open log file for writing.";
    }
}

/* sorting a multiple array after its key */

function aasort(&$array, $key, $order = "ASC") {
    $sorter = array();
    $ret = array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii] = $va->{$key};
    }
    if ($order == "ASC") {
        asort($sorter);
    } else {
        arsort($sorter);
    }
    foreach ($sorter as $ii => $va) {
        $ret[$ii] = $array[$ii];
    }
    $array = $ret;
}

function aasort1(&$array, $key, $order = "ASC") {
    $sorter = array();
    $ret = array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii] = $va[$key];
    }
    if ($order == "ASC") {
        asort($sorter);
    } else {
        arsort($sorter);
    }
    foreach ($sorter as $ii => $va) {
        $ret[$ii] = $array[$ii];
    }
    $array = $ret;
}

function datetime_to_text($datetime = "") {
    $unixdatetime = strtotime($datetime);

    return strftime("%B %d, %Y at %I:%M %p", $unixdatetime);
}

function render_layout_template($template = "", $param) {
    ob_start();
    //extract everything in param into the current scope
    extract($param);
    include(SITE_ROOT . DS . 'layouts' . DS . $template);
}

function ndebug($array) {
    echo '<pre>' . print_r($array) . '</pre>';
}

/**
 * Calculates the great-circle distance between two points, with
 * the Haversine formula.
 * @param float $latitudeFrom Latitude of start point in [deg decimal]
 * @param float $longitudeFrom Longitude of start point in [deg decimal]
 * @param float $latitudeTo Latitude of target point in [deg decimal]
 * @param float $longitudeTo Longitude of target point in [deg decimal]
 * @param float $earthRadius Mean earth radius in [m]
 * @return float Distance between points in [m] (same as earthRadius)
 */
function haversineGreatCircleDistance(
$latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
    return $angle * $earthRadius;
}

/**
 * Calculates the great-circle distance between two points, with
 * the Vincenty formula.
 * @param float $latitudeFrom Latitude of start point in [deg decimal]
 * @param float $longitudeFrom Longitude of start point in [deg decimal]
 * @param float $latitudeTo Latitude of target point in [deg decimal]
 * @param float $longitudeTo Longitude of target point in [deg decimal]
 * @param float $earthRadius Mean earth radius in [m]
 * @return float Distance between points in [m] (same as earthRadius)
 */
function vincentyGreatCircleDistance(
$latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $lonDelta = $lonTo - $lonFrom;
    $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
    $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

    $angle = atan2(sqrt($a), $b);
    return $angle * $earthRadius;
}

function schedule_display($schedule) {
    if (strlen($schedule) && $schedule != '00:00-00:00') {
        return $schedule;
    }
    return 'Not available';
}

function get_hours() {
    return array('00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23');
}

function get_minutes() {
    return array('00', '15', '30', '45');
}

function prepare_schedule_for_db($from_hours, $from_minutes, $to_hours, $to_minutes) {
    return $from_hours . ':' . $from_minutes . '-' . $to_hours . ':' . $to_minutes;
}

function prepare_schedule_for_select($schedule) {
    list($from, $to) = explode('-', $schedule);
    list($from_hours, $from_minutes) = explode(':', $from);
    list($to_hours, $to_minutes) = explode(':', $to);

    return array($from_hours, $from_minutes, $to_hours, $to_minutes);
}

function generate_salt($length) {
    // Not 100% unique, not 100% random, but good enough for a salt
    // MD5 returns 32 characters
    $unique_random_string = md5(uniqid(mt_rand(), true));
    // Valid characters for a salt are [a-zA-Z0-9./]
    $base64_string = base64_encode($unique_random_string);
    // But not '+' which is valid in base64 encoding
    $modified_base64_string = str_replace('+', '.', $base64_string);
    // Truncate string to the correct length
    $salt = substr($modified_base64_string, 0, $length);
    return $salt;
}

function encryptPassword($password) {
    $saltLength = 22;
    $hashFormat = "$2y$10$";
    $salt = generate_salt($saltLength);
    $formatAndSalt = $hashFormat . $salt;
    return $hashedPassword = crypt($password, $formatAndSalt);
}
function sendMail($to, $from, $subject, $message){
    $headers = "From: {$from}\n";
    $headers .= "Reply-To: {$from}\n";
    $headers .= "X-Mailer: PHP/".phpversion()."\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-Type: text/html; charset=utf8";
    $result = mail($to, $subject, $message, $headers);
    return $result;
}
function sendSMS($no_destination, $text_sms, $select_format='normal')
{
    // Connect to Gammu database
    mysql_connect("localhost", "root", "irfan0505");
    mysql_select_db("smsgw");
    if ($select_format == "flash") {
    // If 'flash'
    // Query send flash format sms
        $query = "INSERT INTO outbox(DestinationNumber, TextDecoded, CreatorID, Class)
        VALUES ('$no_destination', '$text_sms', 'smsgw', '0')";
    // Execute query
        mysql_query($query);
    } else if ($select_format == "normal") {
    // If 'normal'
    // Query send normal format sms
        $query = "INSERT INTO outbox(DestinationNumber, TextDecoded, CreatorID, Class)
        VALUES ('$no_destination', '$text_sms', 'smsgw', '-1')";
    // Execute  query
        mysql_query($query);
    } else
    {    return 0;}
    return mysql_affected_rows() == 1;
}

function log_request()
{
    global $database;
    $post = mysql_real_escape_string(json_encode($_POST));
    $get = mysql_real_escape_string(json_encode($_GET));
    $files = mysql_real_escape_string(json_encode($_FILES));
    $request = mysql_real_escape_string(json_encode($_REQUEST));
    $sql = "INSERT INTO log_request  "
        ." (post, get, files, request) "
        . "VALUES('". $post ."', '". $get ."', '". $files ."', '". $request ."')";
    $database->query($sql);
}
function log_debug($title, $message , $type='debug')
{
    global $database;
    $title = mysql_real_escape_string($title);
    $type = mysql_real_escape_string($type);
    $message = mysql_real_escape_string($message);
    $sql = "INSERT INTO log_debug  "
        . " (title, type, message) "
        . "VALUES('". $title ."', '". $type ."', '". $message ."' )";
    $database->query($sql);
}
?>