<?php
define('DEVELOPMENT', 1);
define('TESTING', 2);
define('PRODUCTION', 4);
define("ENVIRONMENT", PRODUCTION);
//define("ENVIRONMENT", DEVELOPMENT);

switch (ENVIRONMENT) {

    case DEVELOPMENT:
        error_reporting(E_ALL);
        break;
    case TESTING:
        error_reporting(E_ERROR);
        break;
    case PRODUCTION:
        error_reporting(0);
        break;
}
// Define the core paths
// Define them as absolute paths to make sure that require_once works as expected
// DIRECTORY_SEPARATOR is a PHP pre-defined constant
// (\ for Windows, / for Unix)
defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

defined('SITE_ROOT') ? null :
                //define('SITE_ROOT', DS . 'flex' . DS . 'domain' . DS . 'ifiego.com' . DS . 'site' . DS . 'www' . DS . 'php_ig' . DS . 'searchable-address-irfan');
                define("SITE_ROOT", __DIR__ . DS . '..');
defined('LIB_PATH') ? null : define('LIB_PATH', SITE_ROOT . DS . 'includes');

define('EMAIL_TO', 'ghauriirfan@gmail.com');
define('EMAIL_NAME', 'Irfan Ghauri');
//define('EMAIL_FROM', 'noreply@searchable-address.com');
define('EMAIL_FROM', 'ghauriirfan@gmail.com');
define('EMAIL_FROM_NAME', 'Searchable Address');
define('MAIN_FOLDER', 'searchable-address');

// load config file first
require_once(LIB_PATH . DS . 'constants.php');
require_once(LIB_PATH . DS . 'config.php');

// load basic functions next so that everything after can use them
require_once(LIB_PATH . DS . 'functions.php');

// load core objects
require_once(LIB_PATH . DS . 'session.php');
require_once(LIB_PATH . DS . 'database.php');
require_once(LIB_PATH . DS . "phpMailer" . DS . "class.phpmailer.php");
require_once(LIB_PATH . DS . "phpMailer" . DS . "class.smtp.php");
require_once(LIB_PATH . DS . "phpMailer" . DS . "language" . DS . "phpmailer.lang-en.php");

// load database-related classes
require_once(LIB_PATH . DS . 'user.php');
require_once(LIB_PATH . DS . 'provider.php');
?>